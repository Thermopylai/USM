﻿Write-Output "Checking for required packages..."
if (Get-Command python -ErrorAction SilentlyContinue)
{
    Write-Output "Python found"
}
else
{
    Write-Output "Could not find python, is it added to PATH?"
    Write-Output "Cancelling"
    pause
    exit
}
if (Get-Command pip3 -ErrorAction SilentlyContinue)
{
    Write-Output "Pip found"
}
else
{
    Write-Output "Could not find pip, is python installed correctly?"
}
if (Get-Command virtualenv -ErrorAction SilentlyContinue)
{
    "Virtualenv found"
}
else
{
    $confirmation = read-host "Virtualenv could not be found, would you like to install it:" -Confirm
    if($confirmation -eq 'y')
    {
        pip3 install virtualenv
    }
    else { exit }
}

Write-Output "Creating virtual environment..."
if(Get-Item $PSScriptRoot\venv -ErrorAction SilentlyContinue)
{
    Remove-Item $PSScriptRoot\venv -Recurse
}
virtualenv venv
.\venv\Scripts\activate
pip install -r requirements.txt

Write-Output "Creating database..."
if(Get-Item $PSScriptRoot\usm\database\usm.db -ErrorAction SilentlyContinue)
{
    Remove-Item $PSScriptRoot\usm\database\usm.db
}

Write-Output "Running database migrations..."
python $PSScriptRoot\usm\manage.py migrate

Write-Output "Populating database with default data..."
python $PSScriptRoot\usm\manage.py loaddata init.json

deactivate
Write-Output "Done!"