[![pipeline status](https://gitlab.com/Thermopylai/USM/badges/master/pipeline.svg)](https://gitlab.com/Thermopylai/USM/commits/master)

# Install Sqlite 3 (Win 10)
 - Go to https://www.sqlite.org/download.html
 - If you are using Windows select from Precompiled Binaries for Windows and download sqlite-dll-win64-x64-3210000.zip and sqlite-tools-win32-x86-3210000.zip
 - Unzip both files to Appdata\local\programs\sqlite\ so that the executables and binary files are in the same directory
 - Add SQLite folder to your environment variables to the PATH variable
 - Run sqlite3.exe

# Deployment (Win 10)
 - Install python by using the python installation software for windows
   - If powershell doesn't recognize `python` as a command you need to add it to system path
     1. Hold `Win` and press `Pause`
     2. Click 'Advanced system settings'
     3. Click 'Environment variables'
     4. Double click the Path variable
     5. Click 'New'
     6. Add the installation path to python
     7. Click 'New'
     8. Add the 'Scripts' folder in the python directory
 - Open powershell as administrator (windows key and type in powershell)
 - To be able to use virtualenv you have to give a permission to run scripts in powershell. Set the system policy by executing `Set-ExecutionPolicy RemoteSigned` and accept with `A`
 - Navigate the powershell terminal to the USM directory using the `cd` command
 - Run `.\setup.ps1`
 - Activate the virtual environment with `venv\Scripts\activate` and it can be deactivated with `deactivate`
 - To run USM use `python usm\manage.py runserver`
 - Check that the server is working correctly by connecting to localhost:8000 using a browser

# Clearing the database
  - The database can be emptied with by deleting usm\database\usm.db 
  - Recreate the database by running `python .\usm\manage.py migrate`
  - To return the database to it's initial state use `python .\usm\manage.py loaddata init.json`
