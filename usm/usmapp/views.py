# Copyright 2018 Sparta Consulting Oy
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout

from .handlers.startview import StartView
from .handlers.mainview import MainView
from .handlers.helpview import HelpView
from .handlers.profileview import ProfileView
from .handlers.projectview import ProjectView
from .handlers.conceptview import ConceptView
from .handlers.editorview import EditorView
from .handlers.datasearch import DataSearch


def startpage(request):
    """Handles the startpage functionality."""
    return StartView.handle_request(request)


@login_required
def mainpage(request):
    """Handles the mainpage functionality."""
    return MainView.handle_request(request)


@login_required
def helppage(request):
    return HelpView.handle_request(request)


@login_required
def profilepage(request):
    """Handles the profile management."""
    return ProfileView.handle_request(request)


@login_required
def projectpage(request, project_id):
    """Handles the project page functionality."""
    return ProjectView.handle_request(request, project_id)


@login_required
def conceptpage(request, project_id, concept_id):
    """Handles the concept page functionality."""
    return ConceptView.handle_request(request, project_id, concept_id)


@login_required
def concepttool(request, project_id, concept_id):
    return EditorView.handle_request(request, project_id, concept_id)


@login_required
def syslogout(request):
    """Logs the user out."""
    logout(request)
    return HttpResponseRedirect("/")


@login_required
def search_data(request, project_id):
    """Handels data search functionality."""
    return DataSearch.handle_request(request, project_id)
