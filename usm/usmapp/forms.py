# Copyright 2018 Sparta Consulting Oy
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.core import validators


class RegisterForm(UserCreationForm):
    email = forms.CharField(label='Email', max_length=100, validators=[
                            validators.validate_email])
    firstname = forms.CharField(label='First name', max_length=100)
    lastname = forms.CharField(label='Last name', max_length=100)
    username = forms.CharField(widget=forms.TextInput(
        attrs={'id': 'id_username_register', 'autofocus': ''}))


class CreateProjectForm(forms.Form):
    name = forms.CharField(label='Project Name', max_length=100,
                           widget=forms.TextInput(attrs={'autofocus': ''}))


class CreateConceptForm(forms.Form):
    name = forms.CharField(label='Concept Name', max_length=100,
                           widget=forms.TextInput(attrs={'autofocus': ''}))


class CommentForm(forms.Form):
    message = forms.CharField(widget=forms.Textarea(
        attrs={'class': 'form-control col-md-8', 'rows': '2', 'placeholder': 'Write a comment'}))


class ChangeInfoForm(forms.Form):
    firstname = forms.CharField(label='First name', max_length=100)
    lastname = forms.CharField(label='Last name', max_length=100)


class PasswordChangeForm(PasswordChangeForm):
    old_password = forms.CharField(
        label=("Old password"),
        strip=False,
        widget=forms.PasswordInput(attrs={'autofocus': False}),
    )


class FeedbackForm(forms.Form):
    title = forms.CharField(label='Title', max_length=100, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Title'}))
    message = forms.CharField(widget=forms.Textarea(
        attrs={'class': 'form-control', 'rows': '3', 'placeholder': 'Message'}))
