# Copyright 2018 Sparta Consulting Oy
import uuid
from django.contrib.auth.models import User, Permission
from django.db import models


class Comment(models.Model):
    UUID = models.UUIDField(
        primary_key=False, default=uuid.uuid4, editable=False)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    createtime = models.DateTimeField(auto_now_add=True)
    message = models.TextField()
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.message


class Concept_Data(models.Model):
    UUID = models.UUIDField(
        primary_key=False, default=uuid.uuid4, editable=False)
    createtime = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    data = models.TextField(editable=True, blank=True)
    image = models.TextField(blank=True)

    def __str__(self):
        return ("{0} - {1}" .format(self.createtime, self.created_by.username))


class Concept_State(models.Model):
    name = models.CharField(max_length=100)
    UUID = models.UUIDField(
        primary_key=False, default=uuid.uuid4, editable=False)

    def __str__(self):
        return self.name


class Concept(models.Model):
    name = models.CharField(max_length=100)
    UUID = models.UUIDField(
        primary_key=False, default=uuid.uuid4, editable=False)
    createtime = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    datas = models.ManyToManyField(Concept_Data, blank=True)
    comments = models.ManyToManyField(Comment, blank=True)
    state = models.ForeignKey(Concept_State, on_delete=models.CASCADE)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Role(models.Model):
    name = models.CharField(max_length=100)
    UUID = models.UUIDField(
        primary_key=False, default=uuid.uuid4, editable=False)
    permissions = models.ManyToManyField(Permission)

    def __str__(self):
        return self.name


class Project_State(models.Model):
    name = models.CharField(max_length=100)
    UUID = models.UUIDField(
        primary_key=False, default=uuid.uuid4, editable=False)

    def __str__(self):
        return self.name


class Project(models.Model):
    name = models.CharField(max_length=100)
    UUID = models.UUIDField(
        primary_key=False, default=uuid.uuid4, editable=False)
    createtime = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    concepts = models.ManyToManyField(Concept, blank=True)
    state = models.ForeignKey(Project_State, on_delete=models.CASCADE)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Member_Type(models.Model):
    name = models.CharField(max_length=100)
    UUID = models.UUIDField(
        primary_key=False, default=uuid.uuid4, editable=False)
    description = models.TextField(editable=True, blank=True)

    def __str__(self):
        return self.name


class Member(models.Model):
    UUID = models.UUIDField(
        primary_key=False, default=uuid.uuid4, editable=False)
    user_id = models.ForeignKey(
        User, related_name='member', on_delete=models.CASCADE)
    project_id = models.ForeignKey(Project, on_delete=models.CASCADE)
    role_id = models.ForeignKey(Role, on_delete=models.CASCADE)
    type = models.ForeignKey(Member_Type, on_delete=models.CASCADE)
    createtime = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        User, related_name='creator', on_delete=models.CASCADE)

    def __str__(self):
        return ("{0} - {1} - {2}" .format(self.user_id.username, self.project_id.name, self.role_id.name))
