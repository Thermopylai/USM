# Copyright 2018 Sparta Consulting Oy
from django.contrib import admin
from .models import Project, Concept, Member, Member_Type, Role, Comment, Concept_Data, Concept_State, Project_State

admin.site.register(Project)
admin.site.register(Concept)
admin.site.register(Role)
admin.site.register(Member)
admin.site.register(Member_Type)
admin.site.register(Comment)
admin.site.register(Concept_Data)
admin.site.register(Concept_State)
admin.site.register(Project_State)
