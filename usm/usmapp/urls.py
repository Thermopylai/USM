# Copyright 2018 Sparta Consulting Oy
from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('', views.startpage, name='startpage'),
    path('main/', views.mainpage, name='mainpage'),
    path('profile/', views.profilepage, name="profilepage"),
    path('logout/', views.syslogout, name="logout"),
    path('help/', views.helppage, name="help"),

    url(r'^(?P<project_id>[0-9]+)/$', views.projectpage, name="projectpage"),
    url(r'^(?P<project_id>[0-9]+)/(?P<concept_id>[0-9]+)/$',
        views.conceptpage, name="conceptpage"),
    url(r'^(?P<project_id>[0-9]+)/(?P<concept_id>[0-9]+)/edit/$',
        views.concepttool, name="concepttool"),
    url(r'^(?P<project_id>[0-9]+)/search_status/',
        views.search_data, name="search_data"),
]
