from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User

from ..models import Project, Concept, Member, Role, Member_Type


class DataSearch():

    request = 0
    project = Project()
    memberlist = []
    project_members = []

    def handle_request(request, project_id):
        print("Handle request datasearch")
        handle = DataSearch()
        handle.request = request
        handle.project = get_object_or_404(Project, pk=project_id)
        handle.project_members = User.objects.filter(
            member__in=Member.objects.filter(project_id=handle.project.id))

        if request.method == 'POST':
            if 'ajax-search' in request.POST:
                return handle.form_switch(request.POST['ajax-search'])

        return "hello"

    def search_data(self):
        search_text = self.request.POST['search_text']
        if search_text != '':
            users = User.objects.filter(username__istartswith=search_text).exclude(
                username=self.request.user.username)
        else:
            users = ''

        return render(self.request, 'usmapp/ajax_search.html', {'users': users})

    def add_member_invitelist(self):
        member = User.objects.get(username=self.request.POST['search_text'])
        if member in self.memberlist or member in self.project_members or member == self.request.user:
            print("Member already in the list")
        else:
            self.memberlist.append(member)
            print(self.memberlist)

        return render(self.request, 'usmapp/ajax_search.html', {'memberlist': self.memberlist})

    def remove_member_invitelist(self):
        print("remove user django")
        member = User.objects.get(username=self.request.POST['remove_member'])
        self.memberlist.remove(member)

        return render(self.request, 'usmapp/ajax_search.html', {'memberlist': self.memberlist})

    def addmember_project(self):
        print("Adding a new member")
        print(self.memberlist)
        role = Role.objects.get(name='Light User')
        for member in self.memberlist:
            member = Member(
                user_id=member,
                project_id=self.project,
                role_id=role,
                type=Member_Type.objects.get(name="Default"),
                created_by=self.request.user
            )
            member.save()
        print("Added a member")
        self.memberlist.clear()
        return HttpResponseRedirect('/{0}/' .format(self.project.id))

    def form_switch(self, id):
        switcher = {
            'member-username': self.search_data,
            'Add Member': self.add_member_invitelist,
            'Remove User': self.remove_member_invitelist,
            'Send Invites': self.addmember_project,
        }
        func = switcher.get(id, lambda: "nothing")
        return func()
