from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponseNotFound
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages

from ..models import Project, Concept, Member, Role, Project_State, Concept_State
from ..forms import CreateConceptForm


class ProjectView():

    LIST_SIZE = 6  # Determines how many list elements there can be before making another page

    request = 0
    project = Project()
    permission_list = []

    def handle_request(request, project_id):
        handle = ProjectView()
        handle.request = request
        handle.project = get_object_or_404(Project, pk=project_id)
        concepts = handle.project.concepts.all().filter(
            deleted=False).order_by('createtime').reverse()
        members = Member.objects.filter(project_id=project_id)
        member_list = []
        modified_list = []
        roles = Role.objects.all()
        role = Member.objects.get(
            user_id=request.user, project_id=project_id).role_id
        permission_queryset = role.permissions.all()
        project_states = Project_State.objects.all()
        handle.permission_list = []

        paginator = Paginator(concepts, handle.LIST_SIZE)
        page = request.GET.get('page')
        concepts = paginator.get_page(page)

        for permission in permission_queryset:
            handle.permission_list.append(permission.name)
        for member in members:
            user = User.objects.get(pk=member.user_id.id)
            user = ("{0} {1}" .format(user.first_name, user.last_name))
            dict = {'id': member.user_id.id, 'name': user, 'role': member.role_id.id,
                    'createtime': member.createtime, 'uuid': member.UUID}
            member_list.append(dict)
        for concept in concepts:
            if concept.datas.all().order_by('createtime').reverse().first():
                date = concept.datas.all().order_by('createtime').reverse().first().createtime
                concept.modified = date
        createconceptform = CreateConceptForm()

        if not Member.objects.filter(project_id=project_id, user_id=request.user.id).exists() or handle.project.deleted:
            print("Not allowed to view this project")
            return HttpResponseRedirect('/')

        if request.method == 'POST':
            if 'button' in request.POST:
                print("Handling a form")
                return handle.form_switch(request.POST['button'])
            elif 'role' in request.POST:
                user = User.objects.get(pk=request.POST['user'])
                member = Member.objects.get(
                    project_id=handle.project, user_id=user)
                role = Role.objects.get(UUID=request.POST['role'])
                member.role_id = role
                member.save()
                print("Changing role")
                return HttpResponseRedirect('/{0}/' .format(project_id))

        if handle.project.state.name == 'Finished':
            return render(request, 'usmapp/project_released.html', {
                'dates': modified_list,
                'project': handle.project,
                'concepts': concepts,
                'users': member_list,
                'roles': roles,
                'permissions': handle.permission_list,
                'project_states': project_states})
        elif handle.project.state.name == 'BLOCKED':
            return render(request, 'usmapp/project_blocked.html', {
                'dates': modified_list,
                'project': handle.project,
                'concepts': concepts,
                'users': member_list,
                'roles': roles,
                'permissions': handle.permission_list,
                'project_states': project_states})

        print("Rendering project view")
        return render(request, 'usmapp/project_page.html', {
            'createconceptform': createconceptform,
            'dates': modified_list,
            'project': handle.project,
            'concepts': concepts,
            'users': member_list,
            'roles': roles,
            'permissions': handle.permission_list,
            'project_states': project_states})

    def handle_create_concept(self):
        createconceptform = CreateConceptForm(self.request.POST)
        if createconceptform.is_valid():
            concept = Concept(
                name=createconceptform.cleaned_data['name'],
                state=Concept_State.objects.get(name="In Development"),
                created_by=self.request.user,
            )
            concept.save()
            self.project.concepts.add(concept)
        print("Creating concept")
        messages.success(self.request, 'Concept created!')
        return HttpResponseRedirect('/{0}/' .format(self.project.id))

    def handle_delete_project(self):
        self.project.deleted = True
        self.project.save()
        print("Deleting project")
        messages.info(self.request, 'Project deleted!')
        return HttpResponseRedirect('/main/')

    def handle_rename_project(self):
        self.project.name = self.request.POST['name']
        self.project.save()
        print("Renaming project")
        messages.success(self.request, 'Project renamed')
        return HttpResponseRedirect('/{0}/' .format(self.project.id))

    def handle_block_project(self):
        self.project.state = Project_State.objects.get(name='BLOCKED')
        self.project.save()
        return HttpResponseRedirect('/{0}/' .format(self.project.id))

    def handle_unblock_project(self):
        self.project.state = Project_State.objects.get(name='In Progress')
        self.project.save()
        return HttpResponseRedirect('/{0}/' .format(self.project.id))

    def handle_release_project(self):
        self.project.state = Project_State.objects.get(name='Finished')
        for concept in self.project.concepts.all().filter(deleted=False):
            if concept.state != Concept_State.objects.get(name='Released'):
                concept.state = Concept_State.objects.get(name="Released")
                concept.save()
        self.project.save()
        return HttpResponseRedirect('/{0}/' .format(self.project.id))

    def handle_search_concepts(self):
        print("concept search start")
        if not self.project.concepts.all().filter(deleted=False).exists():
            return HttpResponseNotFound()
        search_text = self.request.POST['search_text']
        concepts = Concept.objects.none()
        paginator = Paginator(concepts, self.LIST_SIZE)
        if search_text != '':
            concepts = self.project.concepts.filter(
                name__icontains=search_text)
        else:
            page = self.request.POST.get('page')
            print(page)
            if page is None:
                page = 1
            else:
                page = int(page)
            page = (page - 1) * self.LIST_SIZE
            concepts = self.project.concepts.all().order_by(
                'createtime').reverse()[page:]

        paginator.object_list = concepts
        page = self.request.GET.get('page')
        concepts = paginator.get_page(page)

        return render(self.request, 'usmapp/concept_list.html', {
            'concepts': concepts,
            'project': self.project,
            'permissions': self.permission_list})

    def handle_remove_member(self):
        member = Member.objects.get(UUID=self.request.POST['member_uuid'])
        member.delete()
        return HttpResponseRedirect('/main/')

    def form_switch(self, button):
        switcher = {
            'Create': self.handle_create_concept,
            'Delete': self.handle_delete_project,
            'Rename': self.handle_rename_project,
            'Block': self.handle_block_project,
            'Unblock': self.handle_unblock_project,
            'Release': self.handle_release_project,
            'searchproject-input': self.handle_search_concepts,
            'Remove': self.handle_remove_member,
        }

        func = switcher.get(button, lambda: "nothing")
        return func()
