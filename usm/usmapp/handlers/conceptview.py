from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages

from ..models import Project, Concept, Member, Comment, Concept_State
from ..forms import CommentForm


class ConceptView():
    request = 0
    project = Project()
    concept = Concept()

    def handle_request(request, project_id, concept_id):
        handle = ConceptView()
        handle.request = request
        handle.concept = get_object_or_404(Concept, pk=concept_id)
        handle.project = get_object_or_404(Project, pk=project_id)
        comments = handle.concept.comments.all()
        datas = handle.concept.datas.all().order_by('createtime').reverse()
        first_data = handle.concept.datas.all().order_by('createtime').reverse().first()
        member = Member.objects.get(
            user_id=request.user, project_id=project_id)
        role = member.role_id
        permission_list = []
        permission_queryset = role.permissions.all()
        concept_states = Concept_State.objects.all()
        for permission in permission_queryset:
            permission_list.append(permission.name)
        commentform = CommentForm()

        if (not Member.objects.filter(project_id=project_id, user_id=request.user.id).exists()
                or handle.concept.deleted or handle.concept.state.name == 'BLOCKED' and 'Can change project' not in permission_list):
            return HttpResponseRedirect('/')

        if request.method == 'POST':
            if 'button' in request.POST:
                return handle.form_switch(request.POST['button'])

        if handle.project.state.name == 'BLOCKED':
            return render(request, 'usmapp/concept_blocked.html', {
                'first_data': first_data,
                'project': handle.project,
                'concept': handle.concept,
                'comments': comments,
                'commentform': commentform,
                'permission_list': permission_list,
                'concept_states': concept_states})

        if handle.concept.state.name == 'Released':
            return render(request, 'usmapp/concept_released.html', {'project': handle.project, 'concept': handle.concept, 'first_data': first_data})
        elif handle.concept.state.name == 'BLOCKED':
            return render(request, 'usmapp/concept_blocked.html', {
                'first_data': first_data,
                'project': handle.project,
                'concept': handle.concept,
                'comments': comments,
                'commentform': commentform,
                'permission_list': permission_list,
                'concept_states': concept_states})

        return render(request, 'usmapp/concept_page.html', {
            'first_data': first_data,
            'datas': datas,
            'project': handle.project,
            'concept': handle.concept,
            'comments': comments,
            'commentform': commentform,
            'permission_list': permission_list,
            'concept_states': concept_states})

    def handle_create_comment(self):
        commentform = CommentForm(self.request.POST)
        if commentform.is_valid():
            comment = Comment(
                created_by=self.request.user,
                message=commentform.cleaned_data['message']
            )
            comment.save()
            self.concept.comments.add(comment)
            messages.success(self.request, 'Comment created')
            return HttpResponseRedirect('/{0}/{1}/' .format(self.project.id, self.concept.id))
        else:
            return HttpResponseRedirect('/{0}/{1}/' .format(self.project.id, self.concept.id))

    def handle_delete_comment(self):
        comment = get_object_or_404(
            Comment, UUID=self.request.POST['comment_uuid'])
        comment.deleted = True
        comment.save()
        messages.info(self.request, 'Comment deleted!')
        return HttpResponseRedirect('/{0}/{1}/' .format(self.project.id, self.concept.id))

    def handle_rename_concept(self):
        self.concept.name = self.request.POST['name']
        self.concept.save()
        messages.success(self.request, 'Concept renamed')
        return HttpResponseRedirect('/{0}/{1}/' .format(self.project.id, self.concept.id))

    def handle_delete_concept(self):
        self.concept.deleted = True
        self.concept.save()
        messages.info(self.request, 'Concept deleted!')
        return HttpResponseRedirect('/{0}/' .format(self.project.id))

    def handle_block_concept(self):
        self.concept.state = Concept_State.objects.get(name='BLOCKED')
        self.concept.save()
        return HttpResponseRedirect('/{0}/{1}/' .format(self.project.id, self.concept.id))

    def handle_unblock_concept(self):
        self.concept.state = Concept_State.objects.get(name='In Development')
        self.concept.save()
        return HttpResponseRedirect('/{0}/{1}/' .format(self.project.id, self.concept.id))

    def handle_release_concept(self):
        self.concept.state = Concept_State.objects.get(name='Released')
        self.concept.save()
        return HttpResponseRedirect('/{0}/{1}/' .format(self.project.id, self.concept.id))

    def form_switch(self, button):
        switcher = {
            'Send': self.handle_create_comment,
            'x': self.handle_delete_comment,
            'Rename': self.handle_rename_concept,
            'Delete': self.handle_delete_concept,
            'Block': self.handle_block_concept,
            'Unblock': self.handle_unblock_concept,
            'Release': self.handle_release_concept,
        }

        func = switcher.get(button, lambda: "nothing")
        return func()
