from django.shortcuts import render
from django.http import HttpResponseRedirect
from ..forms import ChangeInfoForm
from ..forms import PasswordChangeForm


class ProfileView():
    request = 0

    def handle_request(request):
        form = PasswordChangeForm(user=request.user)
        handle = ProfileView()
        handle.request = request
        user = request.user
        if request.method == 'POST':
            if 'button' in request.POST:
                return handle.form_switch(request.POST['button'])
        return render(request, 'usmapp/profile_page.html', {'user': user, 'form': form})

    def handle_name_change(self):
        juusto = 0
        return HttpResponseRedirect('/profile')

    def handle_change_password(self):
        form = PasswordChangeForm(
            data=self.request.POST, user=self.request.user)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/profile')
        else:
            return render(self.request, 'usmapp/profile_page.html', {'user': self.request.user, 'form': form})

    def form_switch(self, button):
        switcher = {
            'Change Password': self.handle_change_password,
            'Change info': self.handle_name_change,
        }

        func = switcher.get(button, lambda: "nothing")
        return func()
