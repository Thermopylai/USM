from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import AuthenticationForm
from ..forms import RegisterForm
from django.contrib.auth.models import User
from django.contrib import messages


class StartView():
    def handle_request(request):
        loginform = AuthenticationForm()
        registerform = RegisterForm()

        if request.user.is_authenticated:
            return HttpResponseRedirect('/main/')

        if request.method == 'POST':
            if request.POST['button'] == 'Register':
                registerform = RegisterForm(request.POST)
                if registerform.is_valid():
                    user = User.objects.create_user(
                        username=registerform.cleaned_data['username'],
                        password=registerform.cleaned_data['password1'],
                        email=registerform.cleaned_data['email'],
                        first_name=registerform.cleaned_data['firstname'],
                        last_name=registerform.cleaned_data['lastname'])
                    user.save()
                    messages.success(request, 'You registered succesfully')
                    return HttpResponseRedirect('/')
            else:
                loginform = AuthenticationForm(request.POST)
                user = authenticate(request,
                                    username=request.POST['username'],
                                    password=request.POST['password'])
                if user is not None:
                    login(request, user)
                    return HttpResponseRedirect('/main/')
                else:
                    messages.error(request, 'Incorrect username or password')
        return render(request, 'usmapp/start_page.html', {'registerform': registerform, 'loginform': loginform})
