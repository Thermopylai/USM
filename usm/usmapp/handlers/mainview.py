from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponseNotFound
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from ..models import Role, Member, Project, Project_State, Member_Type
from ..forms import CreateProjectForm


class MainView():

    LIST_SIZE = 6  # Determines how many list elements there can be before making another page

    request = 0

    def handle_request(request):
        handle = MainView()
        handle.request = request
        user = request.user
        projects = Project.objects.filter(member__in=Member.objects.filter(
            user_id=request.user.id)).filter(deleted=False).order_by('createtime').reverse()
        print(projects)

        paginator = Paginator(projects, handle.LIST_SIZE)
        page = request.GET.get('page')
        projects = paginator.get_page(page)

        if request.method == 'POST':
            print("handle")
            return handle.form_switch(request.POST['button'])

        else:
            form = CreateProjectForm()

        return render(request, 'usmapp/main_page.html', {'form': form, 'projects': projects})

    def handle_create_project(self):
        form = CreateProjectForm(self.request.POST)
        if form.is_valid():
            project = Project(
                name=form.cleaned_data['name'],
                state=Project_State.objects.get(name="In Progress"),
                created_by=self.request.user
            )
            project.save()
            role = Role.objects.get(name="Content Admin")
            member = Member(
                user_id=self.request.user,
                project_id=project,
                role_id=role,
                type=Member_Type.objects.get(name="Default"),
                created_by=self.request.user,
            )
            member.save()
            return HttpResponseRedirect('/main/')

    def handle_search_projects(self):
        print("project search start")
        if not Project.objects.filter(member__in=Member.objects.filter(user_id=self.request.user.id)).filter(deleted=False).exists():
            return HttpResponseNotFound()
        search_text = self.request.POST['search_text']
        projects = Project.objects.none()
        paginator = Paginator(projects, self.LIST_SIZE)
        print(search_text)
        if search_text != '':
            projects = Project.objects.filter(member__in=Member.objects.filter(
                user_id=self.request.user.id)).filter(name__icontains=search_text).filter(deleted=False)
        else:
            page = self.request.POST.get('page')
            print(page)
            if page is None:
                page = 1
            else:
                page = int(page)
            page = (page - 1) * self.LIST_SIZE
            projects = Project.objects.filter(member__in=Member.objects.filter(
                user_id=self.request.user.id)).order_by('createtime').filter(deleted=False).reverse()[page:]

        paginator.object_list = projects
        page = self.request.GET.get('page')
        projects = paginator.get_page(page)

        return render(self.request, 'usmapp/project_list.html', {'projects': projects})

    def form_switch(self, id):
        print(id)
        switcher = {
            'Create': self.handle_create_project,
            'ajax-search': self.handle_search_projects,
        }
        func = switcher.get(id, lambda: "nothing")
        return func()
