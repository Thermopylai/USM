from django.shortcuts import render

from ..forms import FeedbackForm


class HelpView():

    def handle_request(request):
        feedbackform = FeedbackForm()
        return render(request, 'usmapp/help_page.html', {'feedbackform': feedbackform})
