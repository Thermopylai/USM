from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect

from ..models import Project, Concept, Member, Concept_Data


class EditorView():
    def handle_request(request, project_id, concept_id):
        if not Member.objects.filter(project_id=project_id, user_id=request.user.id).exists():
            return HttpResponseRedirect('/')
        concept = get_object_or_404(Concept, pk=concept_id)
        project = get_object_or_404(Project, pk=project_id)
        user = request.user
        if request.method == 'GET' and 'version' in request.GET:
            version = request.GET['version']
            if version is not None and version != '':
                conceptdata = concept.datas.filter(createtime=version).first()
        else:
            conceptdata = concept.datas.all().order_by('createtime').reverse().first()
        member = Member.objects.get(
            user_id=request.user, project_id=project_id)
        role = member.role_id
        permission_list = []
        permission_queryset = role.permissions.all()
        for permission in permission_queryset:
            permission_list.append(permission.name)
        if "Can change concept" not in permission_list or concept.state.name == 'BLOCKED' or concept.state.name == 'Released':
            print('Not allowed to view this page')
            return HttpResponseRedirect("/{0}/{1}/" .format(project_id, concept_id))

        if request.method == 'POST':
            imgdata = request.POST['imgdata']
            save = Concept_Data(
                created_by=request.user,
                data=request.POST['data'],
                image=imgdata
            )
            save.save()
            concept.datas.add(save)
            return HttpResponseRedirect("/{0}/{1}/edit" .format(project_id, concept_id))

        return render(request, 'usmapp/concept_tool_page.html', {'project': project, 'concept': concept, 'user': user, 'conceptdata': conceptdata})
