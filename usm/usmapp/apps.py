# Copyright 2018 Sparta Consulting Oy
from django.apps import AppConfig


class UsmappConfig(AppConfig):
    name = 'usmapp'
