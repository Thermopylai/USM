var attributes = {};
jsPlumb.ready(function () {

    var instance = jsPlumb.getInstance({
        Endpoint: ["Blank", {radius: 0}],
        Connector:"Straight",
        ConnectionOverlays: [
            [ "Arrow", {
                location: 1,
                id: "arrow",
                length: 12,
                width: 7,
                foldback: 1
            } ]
        ],
        Container: "canvas"
    });
    var textConnector = {
        connector:"Straight",
        overlays: [
            [ "Label", { label: "...", id: "label", cssClass: "aLabel" }]
        ]
    };

    instance.registerConnectionType("regularArrow", { connector:"Straight" });
    instance.registerConnectionType("textArrow", textConnector);

    window.jsp = instance;

    var canvas = document.getElementById("canvas");
    var rect = document.getElementById("rect");
    var round = document.getElementById("round");
    var elli = document.getElementById("elli");
    var nodes = [];
    var labels = [];
    var connection = "regularArrow";
    var shape = "rectangle";
    var arrowStyle = "solidArrow";

    instance.bind("connection", function (info) {
        instance.repaintEverything();
          var overlayObject = info.connection.getOverlays();
          for (var key in overlayObject) {
          if (key == 'label') {
              var clabel = overlayObject[key].getElement().style;
              addLabel(info, clabel);
          }
      }
    });

    var scale = 1;
    var panning = false;
    var panstart = {};
    var panstop = {x : 0, y : 0};
    var pandiv;

    $("#canvas-container").mouseenter(function(){
        disableScroll();
    });
    $("#canvas-container").mouseleave(function(){
        enableScroll();
    });

    $('#canvas-container').bind('wheel mousewheel', function(e){
        var delta;

        if(e.originalEvent.wheelDelta !== undefined){
            delta = e.originalEvent.wheelDelta;
        }
        else {
            delta = e.originalEvent.deltaY * -1;
        }

        if(delta > 0){
            scale += 0.05;
        }
        else {
            scale -= 0.05;
        }
        $("#canvas").css("transform", "scale(" + scale + ")");
        instance.setZoom(scale);
      });

    instance.on("#canvas-container", "mousedown", function(e) {
        switch(e.which) {
              case 3:
                  pandiv = document.getElementById("canvas");
                  panstart = {x : e.x, y : e.y - 75};
                  panning = true;
                  break;
        }

    })
    .on("#canvas-container", "mousemove", function(e) {
          switch(e.which) {
              case 3:
                  if(!panning) return;
                  pandiv.style.left = (panstop.x + (e.x - panstart.x)) + "px";
                  pandiv.style.top = (panstop.y + ((e.y - 75) - panstart.y)) + "px";

                  break;
          }
    })
    .on("#canvas-container", "mouseup", function(e) {
        switch(e.which){
          case 3:
              if(!panning) return;
              panning = false;
              panstop = {x : parseFloat(pandiv.style.left.substring(0, pandiv.style.left.length - 2)), y : parseFloat(pandiv.style.top.substring(0, pandiv.style.top.length - 2))};
              break;
        }
    });

    var initNode = function(el, isNew) {

          if (isNew) {
              var selectedStyle = checkarrowStyle(arrowStyle);
              instance.makeSource(el, {
                  filter: ".ep",
                  anchor: "Continuous",
                  connectorStyle: selectedStyle,
                  connectionType:connection,
                  extract:{
                      "action":"the-action"
                  }
              });

              instance.makeTarget(el, {
                  dropOptions: { hoverClass: "dragHover" },
                  anchor: "Continuous",
                  allowLoopback: false
              });
          }

          instance.fire("jsPlumbNodeAdded", el);
      };

      var addLabel = function(info, clabel) {
        setTimeout(function(){

            var left = parseFloat(clabel.left.substring(0,clabel.left.length-2));
            var top = parseFloat(clabel.top.substring(0,clabel.top.length-2));
            left = left - 35;
            top = top - 15;
            var label = newNode(shape, left, top, true);
            var selectedStyle = checkarrowStyle(arrowStyle);
            if (info.source.classList.contains("labelWindow")) {
                instance.connect({
                    source: info.source.id,
                    target: label.id,
                    anchors: ["Center", "Center"],
                    paintStyle: selectedStyle,
                    type: "regularArrow",
                });
                instance.connect({
                    source: label,
                    target: info.target,
                    anchors: ["Center", "Continuous"],
                    paintStyle: selectedStyle,
                    type: "regularArrow"
                });
            }
            else if (info.target.classList.contains("labelWindow")) {
                instance.connect({
                    source: info.source.id,
                    target: label.id,
                    anchors: ["Continuous", "Center"],
                    paintStyle: selectedStyle,
                    type: "regularArrow",
                });
                instance.connect({
                    source: label,
                    target: info.target,
                    anchors: ["Center", "Center"],
                    paintStyle: selectedStyle,
                    type: "regularArrow"
                });
            }
            else {
                instance.connect({
                    source: info.source.id,
                    target: label.id,
                    anchors: ["Continuous", "Center"],
                    paintStyle: selectedStyle,
                    type: "regularArrow",
                });
                instance.connect({
                    source: label,
                    target: info.target,
                    anchors: ["Center", "Continuous"],
                    paintStyle: selectedStyle,
                    type: "regularArrow"
                });
            }
            instance.deleteConnection(info.connection);
        }, 50);
    }

      var initLabel = function(el) {

        var selectedStyle = checkarrowStyle(arrowStyle);
              instance.makeSource(el, {
                  filter: ".ep",
                  anchor: "Center",
                  connectorStyle: selectedStyle,
                  connectionType:connection,
                  extract:{
                      "action":"the-action"
                  }
              });

              instance.makeTarget(el, {
                  dropOptions: { hoverClass: "dragHover" },
                  anchor: "Center",
                  allowLoopback: false
              });
      }


      var newNode = function(shape, x, y, islabel, load) {
        var d = document.createElement("div");
        var desc = document.createElement("input");
        desc.setAttribute("type", "hidden");
        desc.setAttribute("id", "textvalue");
        desc.setAttribute("value", "");
        d.classList.add("selectable");
        var remove = document.createElement("div");
        var span = document.createElement("span");
        if (islabel) {
            d.classList.add("labelWindow");
            span.classList.add("labelText");
        }
        else {
            d.classList.add("window");
            span.classList.add("windowText");
        }

        var atbname = document.createElement("input");
        var atbvalue = document.createElement("input");

        span.classList.add("windowText");
        var id = jsPlumbUtil.uuid();
        if (shape == "rectangle") d.classList.add("rectangle");
        if (shape == "round") d.classList.add("round");
        if (shape == "ellipsis") d.classList.add("ellipsis");
        d.appendChild(span);
        d.appendChild(desc);
        if (load == null) {
            span.innerHTML = "...";
            d.id = id;
            d.style.left = x + "px";
            d.style.top = y + "px";
            d.style.backgroundColor = "#ffffff";
            d.style.color = "#000000";
            d.style.borderRadius = "";
            desc.value = "";
        }
        else {
            span.innerHTML = load[1];
            d.id = load[0];
            d.style.top = y;
            d.style.left = x;
            d.style.height = load[2] + "px";
            d.style.width = load[3] + "px";
            d.style.backgroundColor = load[4];
            d.style.color = load[5];
            load[6] == true ? d.classList.add("boxshadow-checked") : d.classList.remove("boxshadow-checked");
            d.style.borderRadius = load[7];
            desc.value = load[8];
        }

        instance.getContainer().appendChild(d);
        nodes.push(d);
        if (islabel) {
            initLabel(d);
        }
        else {
            initNode(d, true);
        }
        return d;
    };

    var checkarrowStyle = function(aStyle, aColor) {
        var style;
        if (aStyle == "solidArrow") {
            if (aColor != null) {
                style = { stroke: aColor, strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4 };
                return style;
            }
            else {
                style = { stroke: "#3671d1", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4 };
                return style;
            }
        }
        else if (aStyle == "dashedArrow") {
            if (aColor != null) {
                style = { stroke: aColor, strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4, dashstyle: "4 2" };
                return style;
            }
            else {
                style = { stroke: "#3671d1", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4, dashstyle: "4 2" };
                return style;
            }
        }
        else if (aStyle == "pointedArrow") {
            if (aColor != null) {
                style = { stroke: aColor, strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4, dashstyle: "1 2" };
                return style;
            }
            else {
                style = { stroke: "#3671d1", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4, dashstyle: "1 2" };
                return style;
            }
        }
    }

    var refreshSources = function() {
        var style = checkarrowStyle(arrowStyle);
            for (i=0; i<nodes.length;i++) {
                instance.unmakeSource(nodes[i]);
                  if (nodes[i].classList.contains("window")) {
                    instance.makeSource(nodes[i], {
                        filter: ".ep",
                        anchor: "Continuous",
                        connectorStyle: style,
                        connectionType:connection,
                        extract:{
                            "action":"the-action"
                        }
                    });
                    instance.makeTarget(nodes[i], {
                        dropOptions: { hoverClass: "dragHover" },
                        anchor: "Continuous",
                        allowLoopback: false
                    });
                  }
                  else {
                    instance.makeSource(nodes[i], {
                        filter: ".ep",
                        anchor: "Center",
                        connectorStyle: style,
                        connectionType:connection,
                        extract:{
                            "action":"the-action"
                        }
                    });
                    instance.makeTarget(nodes[i], {
                        dropOptions: { hoverClass: "dragHover" },
                        anchor: "Center",
                        allowLoopback: false
                    });
                  }
                }
    }

    instance.batch(function () {
        if (document.getElementById("data-json").value != "") {
            var data = JSON.parse(document.getElementById("data-json").value);
            if (Object.keys(data).length > 0) {
                for (var key in data.nodes) {
                    var thisNode = data.nodes[key];
                    if (thisNode.islabel == 1) {
                        newNode(thisNode.shape, thisNode.left, thisNode.top, true, [thisNode.id, thisNode.text, thisNode.height, thisNode.width,
                            thisNode.backgroundcolor, thisNode.color, thisNode.boxshadow, thisNode.borderradius, thisNode.desc]);
                        if (thisNode.attributes != null){
                            attributes[thisNode.id] = thisNode.attributes;
                        }
                    }
                    else {
                        newNode(thisNode.shape, thisNode.left, thisNode.top, false, [thisNode.id, thisNode.text, thisNode.height, thisNode.width,
                            thisNode.backgroundcolor, thisNode.color, thisNode.boxshadow, thisNode.borderradius, thisNode.desc]);
                        if (thisNode.attributes != null){
                            attributes[thisNode.id] = thisNode.attributes;
                        }
                    }
                }
                for (var key in data.connections) {
                    var thisConn = data.connections[key];
                    var thisStyle = checkarrowStyle(thisConn.style, thisConn.color);
                    var conn;
                    if (document.getElementById(thisConn.target).classList.contains("labelWindow")) {
                        conn = instance.connect({
                            source: document.getElementById(thisConn.source),
                            target: document.getElementById(thisConn.target),
                            anchors: ["Continuous", "Center"],
                            paintStyle: thisStyle,
                        });
                    }
                    else if (document.getElementById(thisConn.source).classList.contains("labelWindow")) {
                        conn = instance.connect({
                            source: thisConn.source,
                            target: thisConn.target,
                            anchors: ["Center", "Continuous"],
                            paintStyle: thisStyle,
                        });
                    }
                    else {
                        conn = instance.connect({
                            source: thisConn.source,
                            target: thisConn.target,
                            paintStyle:thisStyle,
                            anchor: "Continuous",

                        });
                    }
                    if (thisConn.desc != null) {
                        conn.setParameter("desc", thisConn.desc);
                    }
                }
                var mintop = Number.POSITIVE_INFINITY;
                var minleft = Number.POSITIVE_INFINITY;
                for(key in data.nodes){
                    var node = data.nodes[key];
                    var nodeleft = parseFloat(node.left.substring(0, node.left.length - 2));
                    var nodetop = parseFloat(node.top.substring(0, node.top.length - 2));
                    if (nodetop < mintop) mintop = nodetop;
                    if (nodeleft < minleft) minleft = nodeleft;
                }
                var pandiv = document.getElementById("canvas");
                pandiv.style.top = (-1 * (mintop - 10)) + "px";
                pandiv.style.left = (-1 * (minleft - 10)) + "px";
                panstop = {x : (-1 * (minleft - 10)), y : (- 1 * (mintop - 10))};
            }
        }
    });

    instance.repaintEverything();
    jsPlumb.fire("jsPlumbLoaded", instance);

});

var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}
function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}
function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove  = preventDefault; // mobile
    document.onkeydown  = preventDefaultForScrollKeys;
}

  function enableScroll() {
      if (window.removeEventListener)
          window.removeEventListener('DOMMouseScroll', preventDefault, false);
      window.onmousewheel = document.onmousewheel = null;
      window.onwheel = null;
      window.ontouchmove = null;
      document.onkeydown = null;
}