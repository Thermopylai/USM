var selectedWindow = "";
jsPlumb.ready(function () {

    var unsaved = function () { return true; };

    var instance = jsPlumb.getInstance({
        Endpoint: ["Dot", { radius: 4 }],
        Connector: "Straight",
        HoverPaintStyle: { strokeWidth: 3 },
        ConnectionOverlays: [
            ["Arrow", {
                location: 1,
                id: "arrow",
                length: 12,
                width: 7,
                foldback: 1
            }]
        ],
        Container: "canvas"
    });
    var textConnector = {
        connector: "Straight",
        overlays: [
            ["Label", { label: "...", id: "label", cssClass: "aLabel" }]
        ]
    };

    instance.registerConnectionType("regularArrow", { connector: "Straight" });
    instance.registerConnectionType("textArrow", textConnector);

    window.jsp = instance;

    var canvas = document.getElementById("canvas");
    var container = document.getElementById("canvas-container");
    var rect = document.getElementById("rect");
    var round = document.getElementById("round");
    var elli = document.getElementById("elli");
    var nodes = [];
    var labels = [];
    var connection = "regularArrow";
    var shape = "rectangle";
    var arrowStyle = "solidArrow";


    instance.bind("connectionDetached", function (c) {
        setSavedState(false);
    });

    instance.bind("click", function (event) {
        refreshRelationMenu(event);
    });

    instance.bind("connection", function (info) {
        instance.repaintEverything();
        var overlayObject = info.connection.getOverlays();
        for (var key in overlayObject) {
            if (key == 'label') {
                var clabel = overlayObject[key].getElement().style;
                addLabel(info, clabel);
            }
        }
        setSavedState(false);
    });
    instance.bind("endpointClick", function (ep) {
        var connection = ep.connections[0];
        if (connection.source == ep.element) {
            //source point clicked
            if (connection.getOverlay("sourceArrow") == null) {
                connection.addOverlay(["Arrow", { location: 0, direction: -1, id: "sourceArrow", length: 14, foldback: 1 }]);
            }
            else {
                connection.removeOverlay("sourceArrow");
            }
        }
        else if (connection.target == ep.element) {
            if (connection.getOverlay("arrow") == null) {
                connection.addOverlay(["Arrow", { location: 1, id: "arrow", length: 12, width: 7, foldback: 1 }]);
            }
            else {
                connection.removeOverlay("arrow");
            }

        }
        setSavedState(false);
    });
    var selecting = false;
    var startx;
    var starty;
    var selectiondiv;
    var selection = [];
    var scale = 1;
    var panning = false;
    var panstart = {};
    var panstop = { x: 0, y: 0 };
    var pandiv;

    document.addEventListener("contextmenu", event => event.preventDefault());
    $('#canvas-container').bind('wheel mousewheel', function (e) {
        var delta;

        if (e.originalEvent.wheelDelta !== undefined) {
            delta = e.originalEvent.wheelDelta;
        }
        else {
            delta = e.originalEvent.deltaY * -1;
        }

        if (delta > 0) {
            scale += 0.05;
        }
        else {
            scale -= 0.05;
        }
        $("#canvas").css("transform", "scale(" + scale + ")");
        instance.setZoom(scale);
    });

    instance.on("#canvas-container", "mousedown", function (e) {
        switch (e.which) {
            case 1:
                if (e.target.id != "canvas-container") { return; }
                selection.length = 0;
                instance.clearDragSelection();
                [].forEach.call(nodes, function (node) {
                    node.classList.remove("multiSelected");
                });
                selecting = true;
                startx = e.x;
                starty = (e.y - 75);
                console.log("Selection box coords: " + startx + ", " + starty);
                console.log("Pan delta: " + panstop.x + ", " + panstop.y);
                selectiondiv = document.createElement("div");
                selectiondiv.classList.add("selectionBox");
                selectiondiv.style.left = (((1 / scale) * (startx - panstop.x))) + "px";
                selectiondiv.style.top = (((1 / scale) * (starty - panstop.y))) + "px";
                selectiondiv.style.width = "10px";
                instance.getContainer().appendChild(selectiondiv);
                break;
            case 3:
                pandiv = document.getElementById("canvas");
                panstart = { x: e.x, y: e.y - 75 };
                panning = true;
                break;
        }

    })
        .on("#canvas-container", "mousemove", function (e) {
            switch (e.which) {
                case 1:
                    if (!selecting) { return; }
                    selectiondiv.style.width = ((1 / scale) * (e.x - startx)) + "px";
                    selectiondiv.style.height = ((1 / scale) * ((e.y - 75) - starty)) + "px";
                    console.log(selectiondiv.style.width);
                    break;
                case 3:
                    if (!panning) return;
                    pandiv.style.left = (panstop.x + (e.x - panstart.x)) + "px";
                    pandiv.style.top = (panstop.y + ((e.y - 75) - panstart.y)) + "px";

                    break;
            }
        })
        .on("#canvas-container", "mouseup", function (e) {
            switch (e.which) {
                case 1:
                    if (!selecting) { return; }
                    selection = [];
                    var stopx = e.x;
                    var stopy = (e.y - 75);
                    [].forEach.call(nodes, function (node) {
                        var nodex = scale * parseFloat(node.style.left.substring(0, node.style.left.length - 2));
                        var nodey = scale * parseFloat(node.style.top.substring(0, node.style.top.length - 2));
                        if (nodex > (startx - panstop.x) && nodex < (stopx - panstop.x)) {
                            if (nodey > (starty - panstop.y) && nodey < (stopy - panstop.y)) {
                                node.classList.add("multiSelected");
                                instance.addToDragSelection(node);
                                selection.push(node);
                            }
                        }
                    });
                    var removeselect = true;
                    [].forEach.call(selection, function (node) {
                        if (node == selectedWindow) {
                            var removeselect = false;
                        }
                    });
                    if (removeselect && $(selectedWindow).is("div")) {
                        refreshPropertyMenu(selectedWindow);
                    }
                    if (selection.length > 0) {
                        if (removeselect && $(selectedWindow).is("div")) {
                            selectedWindow.classList.remove("selectedWindow");
                        }
                        refreshPropertyMenu(selection);
                    }
                    else {
                        if ($(selectedWindow).is("div")) {
                            refreshPropertyMenu(selectedWindow);
                        }
                        else {
                            closeNav();
                        }
                    }
                    selecting = false;
                    selectiondiv.remove();
                    break;
                case 3:
                    if (!panning) return;
                    panning = false;
                    panstop = { x: parseFloat(pandiv.style.left.substring(0, pandiv.style.left.length - 2)), y: parseFloat(pandiv.style.top.substring(0, pandiv.style.top.length - 2)) };
                    break;
            }
        });



    $("#canvas-container").dblclick(function (e) {
        if (e.target.id == "canvas-container") {
            newNode(shape, (1 / scale) * (e.offsetX - panstop.x), (1 / scale) * (e.offsetY - panstop.y), false);
        }
    });

    $(".shape").click(function (e) {
        var id = e.target.id;
        if (id != shape) {
            var shapes = document.querySelectorAll(".shape");
            [].forEach.call(shapes, function (el) {
                el.classList.remove("selected");
            });
            e.target.classList.add("selected");
            shape = id;
        }

    });

    /* Save functions */

    $("#saveButton").click(function (e) {
        saveConcept();
    });

    $(".arrowSelector").click(function (e) {
        var id = e.target.id;
        if (id != connection) {
            var arrows = document.querySelectorAll(".arrowSelector");
            [].forEach.call(arrows, function (el) {
                el.classList.remove("selected");
            });
            e.target.parentElement.classList.add("selected");
            if (connection == "regularArrow") {
                connection = "textArrow";
            }
            else {
                connection = "regularArrow";
            }
            refreshSources();
        }
    });

    $(".arrowstyleSelector").click(function (e) {
        var id = e.target.id;
        if (id != arrowStyle) {
            var arrows = document.querySelectorAll(".arrowstyleSelector");
            [].forEach.call(arrows, function (el) {
                el.classList.remove("selected");
            });
            e.target.parentElement.classList.add("selected");
            arrowStyle = id;
            refreshSources();
        }
    });


    var initNode = function (el, isNew) {
        console.log("initnode triggered");
        $("#canvas-container, .window").mousedown(function (e) {
            if ($("#nodecontext").has(e.target).length > 0) {

            }
            else {
                hideContext();
            }
        });

        $(".window").unbind("contextmenu").contextmenu(function () {
            for (i = 0; i < nodes.length; i++) {
                nodes[i].classList.remove("selectedWindow");
            }
            selectedWindow = $(this)[0];
            selectedWindow.classList.add("selectedWindow");
            //contextBox($(this));
            return false;
        });


        $("#canvas-container").unbind("mousedown").mousedown(function (e) {
            if (!e.target.classList.contains("contextli")) {
                hideContext();

                var shapes = document.querySelectorAll(".window");
                [].forEach.call(shapes, function (el) {
                    el.classList.remove("selectedWindow");
                });

                if (e.target.id == "canvas-container") {
                    selectedWindow = "";
                    [].forEach.call(shapes, function (el) {
                        el.classList.remove("selectedWindow");
                    });
                }
            }
        });

        $(".right-panel").unbind("click").click(function (e) {
            if (selectedWindow != "") {
                selectedWindow.classList.add("selectedWindow");
            }

        });

        $(".window").unbind("mousedown").mousedown(function () {
            for (i = 0; i < nodes.length; i++) {
                nodes[i].classList.remove("selectedWindow");
            }
            selectedWindow = $(this)[0];
            selectedWindow.classList.add("selectedWindow");
            var j = 0;
            for (var i = 0; i < selection.length; i++) {
                if (selection[i] != selectedWindow) {
                    j++;
                }
            }
            if (i == j) {
                [].forEach.call(nodes, function (node) {
                    node.classList.remove("multiSelected");
                });
                instance.clearDragSelection();
                refreshPropertyMenu(selectedWindow);
            }

        });
        $(document).keydown(function (e) {
            if ($(e.target).closest("input")[0]) {
                return;
            }
            if (e.which == 46 && selectedWindow != "") {
                deleteNode();
            }
        });

        instance.draggable(el, {
            containment: false,
            filter: ".ui-resizable-handle, .edit, .delete"
        });

        if($(".window").resizable("instance") !== undefined) {
            console.log("found resizables");
            console.log($(".window").resizable("instance"));
            $(".window").resizable({ disabled: true });
            $(".window").resizable("destroy");
        }

        $(".window").resizable({
            helper: "ui-resizable-helper",
            stop: function (event, ui) {
                instance.repaintEverything();
            }
        });

        $("#contextEdit").unbind("click").click(function () {
            var span = $(selectedWindow).find("span");
            var text = span.text();
            if (selectedWindow.classList.contains("labelWindow")) {
                editnodeText(span[0], selectedWindow, text, true);
            }
            else {
                editnodeText(span[0], selectedWindow, text, false);
            }
            hideContext();
        });

        $("#contextDelete").unbind("click").click(function () {
            deleteNode();
            hideContext();
        });
        $("#contextProps").unbind("click").click(function () {
            openNav('e');
            hideContext();
        });

        $(".window").unbind("dblclick").dblclick(function (e) {
            var span, window, text;
            if ($(this).is("span")) {
                span = $(this);
                text = span.text();
                window = $(this).parent()[0];
            }
            else if ($(this).is("div")) {
                span = $(this).find("span");
                window = $(this)[0];
                text = span.text();
            }
            editnodeText(span[0], window, text, false);

        });

        if (isNew) {
            var selectedStyle = checkarrowStyle(arrowStyle);
            instance.makeSource(el, {
                filter: ".ep",
                anchor: "Continuous",
                connectorStyle: selectedStyle,
                connectionType: connection,
                extract: {
                    "action": "the-action"
                }
            });

            instance.makeTarget(el, {
                dropOptions: { hoverClass: "dragHover" },
                anchor: "Continuous",
                allowLoopback: false
            });
        }



        instance.fire("jsPlumbNodeAdded", el);
    };

    var initLabel = function (el) {
        $(".labelWindow").unbind("dblclick").dblclick(function (e) {
            var span, window, text;
            if ($(this).is("span")) {
                span = $(this);
                text = span.text();
                window = $(this).parent()[0];
            }
            else if ($(this).is("div")) {
                span = $(this).find("span");
                window = $(this)[0];
                text = span.text();
            }
            editnodeText(span[0], window, text, true);

        });
        $(".labelWindow").unbind("contextmenu").contextmenu(function () {
            for (i = 0; i < nodes.length; i++) {
                nodes[i].classList.remove("selectedWindow");
            }
            for (i = 0; i < labels.length; i++) {
                labels[i].classList.remove("selectedWindow");
            }
            selectedWindow = $(this)[0];
            selectedWindow.classList.add("selectedWindow");
            //contextBox($(this));
            return false;
        });

        $(".labelWindow").unbind("mousedown").mousedown(function () {
            for (i = 0; i < labels.length; i++) {
                labels[i].classList.remove("selectedWindow");
            }
            for (i = 0; i < nodes.length; i++) {
                nodes[i].classList.remove("selectedWindow");
            }
            selectedWindow = $(this)[0];
            selectedWindow.classList.add("selectedWindow");
            var j = 0;
            for (var i = 0; i < selection.length; i++) {
                if (selection[i] != selectedWindow) {
                    j++;
                }
            }
            if (i == j) {
                [].forEach.call(nodes, function (node) {
                    node.classList.remove("multiSelected");
                });
                instance.clearDragSelection();
                refreshPropertyMenu(selectedWindow);
            }

        });
        instance.draggable(el, {
            containment: false,
            filter: ".ui-resizable-handle"
        });

        var selectedStyle = checkarrowStyle(arrowStyle);
        instance.makeSource(el, {
            filter: ".ep",
            anchor: "Center",
            connectorStyle: selectedStyle,
            connectionType: connection,
            extract: {
                "action": "the-action"
            }
        });

        instance.makeTarget(el, {
            dropOptions: { hoverClass: "dragHover" },
            anchor: "Center",
            allowLoopback: false
        });
    }

    /* Create a node */

    var newNode = function (shape, x, y, islabel, load) {
        var d = document.createElement("div");
        var ep = document.createElement("div");
        var desc = document.createElement("input");
        desc.setAttribute("type", "hidden");
        desc.setAttribute("id", "textvalue");
        desc.setAttribute("value", "");
        d.classList.add("selectable");
        var remove = document.createElement("div");
        var span = document.createElement("span");
        if (islabel) {
            d.classList.add("labelWindow");
            span.classList.add("labelText");
        }
        else {
            d.classList.add("window");
            span.classList.add("windowText");
        }

        var atbname = document.createElement("input");
        var atbvalue = document.createElement("input");



        span.classList.add("windowText");
        ep.classList.add("ep");
        var id = jsPlumbUtil.uuid();
        if (shape == "rectangle") d.classList.add("rectangle");
        if (shape == "round") d.classList.add("round");
        if (shape == "ellipsis") d.classList.add("ellipsis");
        d.appendChild(ep);
        d.appendChild(span);
        d.appendChild(desc);
        if (load == null) {
            span.innerHTML = "...";
            d.id = id;
            d.style.left = x + "px";
            d.style.top = y + "px";
            d.style.backgroundColor = "#ffffff";
            d.style.color = "#000000";
            d.style.borderRadius = "";
            desc.value = "";
        }
        else {
            span.innerHTML = load[1];
            d.id = load[0];
            d.style.top = y;
            d.style.left = x;
            d.style.height = load[2] + "px";
            d.style.width = load[3] + "px";
            d.style.backgroundColor = load[4];
            d.style.color = load[5];
            load[6] == true ? d.classList.add("boxshadow-checked") : d.classList.remove("boxshadow-checked");
            d.style.borderRadius = load[7];
            desc.value = load[8];
        }

        instance.getContainer().appendChild(d);
        nodes.push(d);
        if (islabel) {
            initLabel(d);
        }
        else {
            initNode(d, true);
        }
        setSavedState(false);
        return d;
    };

    var checkarrowStyle = function (aStyle, aColor) {
        var style;
        if (aStyle == "solidArrow") {
            if (aColor != null) {
                style = { stroke: aColor, strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4 };
                return style;
            }
            else {
                style = { stroke: "#3671d1", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4 };
                return style;
            }

        }
        else if (aStyle == "dashedArrow") {
            if (aColor != null) {
                style = { stroke: aColor, strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4, dashstyle: "4 2" };
                return style;
            }
            else {
                style = { stroke: "#3671d1", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4, dashstyle: "4 2" };
                return style;
            }
        }
        else if (aStyle == "pointedArrow") {
            if (aColor != null) {
                style = { stroke: aColor, strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4, dashstyle: "1 2" };
                return style;
            }
            else {
                style = { stroke: "#3671d1", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4, dashstyle: "1 2" };
                return style;
            }
        }

    }

    function contextBox(el) {
        el = el[0];
        var prop = document.getElementById("nodecontext");
        var elsize = [el.clientWidth, el.clientHeight];
        var elpos = [el.offsetLeft, el.offsetTop];
        prop.style.left = (elpos[0] + elsize[0]) + "px";
        prop.style.top = (elpos[1] + elsize[1]) + "px";
        if (nav_open) {
            $("#contextProps").hide();
        }
        else {
            $("#contextProps").show();
        }
        $(".contextBox").slideDown();
        return;
    }

    var deleteNode = function () {
        $("#deleteModal").modal("show");
        $('#deleteModal').on('shown.bs.modal', function (e) {
            $("#deleteButton").focus();
        })
        $("#deleteButton").click(function () {
            for (i = 0; i < nodes.length; i++) {
                if (nodes[i] == selectedWindow) {
                    nodes.splice(i, 1);
                }
            }
            instance.remove(selectedWindow);

            setSavedState(false);
        });
        hideContext();
    }

    var editnodeText = function (span, window, text, islabel)  {
        span.remove();
        var input = document.createElement("input");
        input.setAttribute("type", "text");
        input.classList.add("nodeInput");
        input.value = text;
        window.appendChild(input);
        input.focus();
        input.select();
        input.onfocusout = function () {
            var span = document.createElement("span");
            if (!islabel) {
                span.classList.add("windowText");
            }
            else {
                span.classList.add("labelText");
            }

            span.innerHTML = input.value;
            window.appendChild(span);
            input.remove();
            initNode(window, false);
            instance.repaintEverything();
        }
        input.onkeydown = function (e) {
            if (e.keyCode == 13) {
                input.blur();
            }
            else {
                setSavedState(false);
            }
        }
    }
    var refreshSources = function () {
        var style = checkarrowStyle(arrowStyle);
        for (i = 0; i < nodes.length; i++) {
            instance.unmakeSource(nodes[i]);
            if (nodes[i].classList.contains("window")) {
                instance.makeSource(nodes[i], {
                    filter: ".ep",
                    anchor: "Continuous",
                    connectorStyle: style,
                    connectionType: connection,
                    extract: {
                        "action": "the-action"
                    }
                });
                instance.makeTarget(nodes[i], {
                    dropOptions: { hoverClass: "dragHover" },
                    anchor: "Continuous",
                    allowLoopback: false
                });
            }
            else {
                instance.makeSource(nodes[i], {
                    filter: ".ep",
                    anchor: "Center",
                    connectorStyle: style,
                    connectionType: connection,
                    extract: {
                        "action": "the-action"
                    }
                });
                instance.makeTarget(nodes[i], {
                    dropOptions: { hoverClass: "dragHover" },
                    anchor: "Center",
                    allowLoopback: false
                });
            }
        }
    }

    var prepareImage = function () {
        $("#left-menu").hide();
        $(".right-panel").hide();
        $(".overhead-panel").hide();
        $(".lower-panel").hide();
        for (i = 0; i < nodes.length; i++) {
            nodes[i].classList.remove("selectedWindow");
        }
        function filter(node) {
            if (node.classList == null) {
                return node;
            }
            else if (!node.classList.contains("jtk-endpoint")) {
                return true;
            }
        }

        domtoimage.toPng($("#canvas")[0], { filter: filter })
            .then(function (dataUrl) {
                document.getElementById("image-data").value = dataUrl;
            })
            .catch(function (error) {
                console.error('oops, something went wrong!', error);
            });


    }

    var addLabel = function (info, clabel) {
        setTimeout(function () {

            var left = parseFloat(clabel.left.substring(0, clabel.left.length - 2));
            var top = parseFloat(clabel.top.substring(0, clabel.top.length - 2));
            left = left - 35;
            top = top - 15;
            var label = newNode(shape, left, top, true);
            var selectedStyle = checkarrowStyle(arrowStyle);
            if (info.source.classList.contains("labelWindow")) {
                instance.connect({
                    source: info.source.id,
                    target: label.id,
                    anchors: ["Center", "Center"],
                    paintStyle: selectedStyle,
                    type: "regularArrow",
                });
                instance.connect({
                    source: label,
                    target: info.target,
                    anchors: ["Center", "Continuous"],
                    paintStyle: selectedStyle,
                    type: "regularArrow"
                });
            }
            else if (info.target.classList.contains("labelWindow")) {
                instance.connect({
                    source: info.source.id,
                    target: label.id,
                    anchors: ["Continuous", "Center"],
                    paintStyle: selectedStyle,
                    type: "regularArrow",
                });
                instance.connect({
                    source: label,
                    target: info.target,
                    anchors: ["Center", "Center"],
                    paintStyle: selectedStyle,
                    type: "regularArrow"
                });
            }
            else {
                instance.connect({
                    source: info.source.id,
                    target: label.id,
                    anchors: ["Continuous", "Center"],
                    paintStyle: selectedStyle,
                    type: "regularArrow",
                });
                instance.connect({
                    source: label,
                    target: info.target,
                    anchors: ["Center", "Continuous"],
                    paintStyle: selectedStyle,
                    type: "regularArrow"
                });
            }
            instance.deleteConnection(info.connection);
        }, 50);
    }

    var saveConcept = function () {
        var data = {};
        var windowArray = nodes;
        data.nodes = {};
        for (i = 0; i < windowArray.length; i++) {
            data.nodes[i] = {};
            if (windowArray[i].classList.contains("rectangle")) {
                data.nodes[i].shape = "rectangle";
            }
            if (windowArray[i].classList.contains("round")) {
                data.nodes[i].shape = "round";
            }
            if (windowArray[i].classList.contains("ellipsis")) {
                data.nodes[i].shape = "ellipsis";
            }
            data.nodes[i].id = windowArray[i].id;
            data.nodes[i].left = windowArray[i].style.left;
            data.nodes[i].top = windowArray[i].style.top;
            data.nodes[i].width = windowArray[i].offsetWidth;
            data.nodes[i].height = windowArray[i].offsetHeight;
            data.nodes[i].backgroundcolor = windowArray[i].style.backgroundColor;
            data.nodes[i].color = windowArray[i].style.color;
            windowArray[i].classList.contains("boxshadow-checked") ? data.nodes[i].boxshadow = true : data.nodes[i].boxshadow = false;
            data.nodes[i].borderradius = windowArray[i].style.borderRadius;
            if (windowArray[i].classList.contains("window")) {
                data.nodes[i].islabel = 0;
            }
            else {
                data.nodes[i].islabel = 1;
            }
            data.nodes[i].desc = windowArray[i].querySelector('#textvalue').value;
            if (attributes[windowArray[i].id] != null) {
                data.nodes[i].attributes = attributes[windowArray[i].id];
            }
            var object = jQuery(windowArray[i]);

            var text = object.find("span").text();
            data.nodes[i].text = text;
        }
        data.connections = {};
        var connectionsArray = instance.getAllConnections();
        var style;
        for (i = 0; i < connectionsArray.length; i++) {
            if (connectionsArray[i].connector.path.hasAttribute("stroke-dasharray")) {
                if (connectionsArray[i].connector.path.getAttribute("stroke-dasharray") == "2 4 ") {
                    style = "pointedArrow";
                }
                else if (connectionsArray[i].connector.path.getAttribute("stroke-dasharray") == "8 4 ") {
                    style = "dashedArrow";
                }
            }
            else {
                style = "solidArrow";
            }
            if (connectionsArray[i].getParameter("style") != null) {
                if (connectionsArray[i].getParameter("style") == "solid") {
                    style = "solidArrow";
                }
                else if (connectionsArray[i].getParameter("style") == "dashed") {
                    style = "dashedArrow";
                }
                else if (connectionsArray[i].getParameter("style") == "pointed") {
                    style = "pointedArrow";
                }
                else {
                    style = "solidArrow";
                }
            }
            var rgb = $(connectionsArray[i].connector.path).css("stroke");
            var color = rgb2hex(rgb);
            data.connections[i] = { overlays: {}, source: connectionsArray[i].source.id, target: connectionsArray[i].target.id, style: style, color: color };
            if (connectionsArray[i].getParameter("desc") != null) {
                var desc = connectionsArray[i].getParameter("desc");
                data.connections[i].desc = desc;
            }
            var overlaysArray = connectionsArray[i].getOverlays();
            var thisConnection = data.connections[i];
            for (var key in overlaysArray) {
                if (overlaysArray.hasOwnProperty(key)) {
                    if (key == "label") {
                        thisConnection.overlays.label = overlaysArray[key].label;
                        thisConnection.overlays.labelid = overlaysArray[key].getElement().id;
                    }
                    if (key == 'arrow') {
                        thisConnection.overlays.arrow = key;
                    }
                    if (key == 'sourceArrow') {
                        thisConnection.overlays.sourceArrow = key;
                    }
                }
            }
        }
        setSavedState(true);
        document.getElementById("input-data").value = JSON.stringify(data);
        document.getElementById("save-form").submit();
    }
    /* Load function */

    instance.batch(function () {
        if (document.getElementById("data-json").value != "") {
            var data = JSON.parse(document.getElementById("data-json").value);
            if (Object.keys(data).length > 0) {
                for (var key in data.nodes) {
                    var thisNode = data.nodes[key];
                    if (thisNode.islabel == 1) {
                        newNode(thisNode.shape, thisNode.left, thisNode.top, true, [thisNode.id, thisNode.text, thisNode.height, thisNode.width,
                        thisNode.backgroundcolor, thisNode.color, thisNode.boxshadow, thisNode.borderradius, thisNode.desc]);
                        if (thisNode.attributes != null) {
                            attributes[thisNode.id] = thisNode.attributes;
                        }
                    }
                    else {
                        newNode(thisNode.shape, thisNode.left, thisNode.top, false, [thisNode.id, thisNode.text, thisNode.height, thisNode.width,
                        thisNode.backgroundcolor, thisNode.color, thisNode.boxshadow, thisNode.borderradius, thisNode.desc]);
                        if (thisNode.attributes != null) {
                            attributes[thisNode.id] = thisNode.attributes;
                        }
                    }
                }
                for (var key in data.connections) {
                    var thisConn = data.connections[key];
                    var thisStyle = checkarrowStyle(thisConn.style, thisConn.color);
                    var conn;
                    if (document.getElementById(thisConn.target).classList.contains("labelWindow")) {
                        conn = instance.connect({
                            source: document.getElementById(thisConn.source),
                            target: document.getElementById(thisConn.target),
                            anchors: ["Continuous", "Center"],
                            paintStyle: thisStyle,
                        });
                    }
                    else if (document.getElementById(thisConn.source).classList.contains("labelWindow")) {
                        conn = instance.connect({
                            source: thisConn.source,
                            target: thisConn.target,
                            anchors: ["Center", "Continuous"],
                            paintStyle: thisStyle,
                        });
                    }
                    else {
                        conn = instance.connect({
                            source: thisConn.source,
                            target: thisConn.target,
                            paintStyle: thisStyle,
                            anchor: "Continuous",
                        });
                    }
                    if (thisConn.desc != null) {
                        conn.setParameter("desc", thisConn.desc);
                    }
                }
                var mintop = Number.POSITIVE_INFINITY;
                var minleft = Number.POSITIVE_INFINITY;
                for (key in data.nodes) {
                    var node = data.nodes[key];
                    var nodeleft = parseFloat(node.left.substring(0, node.left.length - 2));
                    var nodetop = parseFloat(node.top.substring(0, node.top.length - 2));
                    if (nodetop < mintop) mintop = nodetop;
                    if (nodeleft < minleft) minleft = nodeleft;
                }
                var pandiv = document.getElementById("canvas");
                pandiv.style.top = (-1 * (mintop - 60)) + "px";
                pandiv.style.left = (-1 * (minleft - 160)) + "px";
                panstop = { x: (-1 * (minleft - 160)), y: (- 1 * (mintop - 60)) };
            }
        }
        setSavedState(true);
    });

    instance.repaintEverything();
    jsPlumb.fire("jsPlumbLoaded", instance);


});

function setSavedState(check) {
    var savetext = document.getElementById("save-state");
    if (check == true) {
        window.onbeforeunload = null;
        savetext.innerHTML = "";
    }
    else {
        window.onbeforeunload = function () { return true; };
        savetext.innerHTML = "You have unsaved changes";
    }
}

