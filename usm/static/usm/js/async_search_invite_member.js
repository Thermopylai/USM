$(function searchMember() {

    $('#searchmember-input').keyup(function () {

        $.ajax({
            type: "POST",
            url: $('#hidden-search-url-username').val(),
            data: {
                'ajax-search': $('#member-username').val(),
                'search_text': $('#searchmember-input').val(),
                'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()
            },
            success: memberSearchSuccess,
            dataType: 'html'
        });
    });
});

function memberSearchSuccess(data, textStatus, jqXHR) {
    $('#search-results').html(data)
}

$(function addMember() {

    $('#add-member-btn').click(function () {

        $.ajax({
            type: "POST",
            url: $('#hidden-search-url-username').val(),
            data: {
                'ajax-search': $('#add-member-btn').val(),
                'search_text': $('#searchmember-input').val(),
                'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()
            },
            success: addSuccess,
            dataType: 'html'
        });
    });
});

function addSuccess(data, textStatus, jqXHR) {
    $('#list-results').html(data)
    $('#search-results').html(null)
    $('#searchmember-input').val(null)

}
function removeMembers(username) {
    console.log("removeMembers");
    $.ajax({
        type: "POST",
        url: $('#hidden-search-url-username').val(),
        data: {
            'ajax-search': $('#remove-member-btn').val(),
            'remove_member': username,
            'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()
        },
        success: removeSuccess,
        dataType: 'html'
    });
}

function removeSuccess(data, textStatus, jqXHR) {
    $('#list-results').html(data)
}

function AddTo(username) {
    var input = document.getElementById("searchmember-input");
    input.value = username;
};