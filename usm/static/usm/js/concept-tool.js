$( document ).ready(function() {

});



jsPlumb.ready(function () {

    $( ".ui-resizable-handle" ).mouseup(function() {
        jsPlumb.repaint();
        console.log("asd");
      });

    // setup some defaults for jsPlumb.
    var instance = jsPlumb.getInstance({
        Endpoint: ["Dot", {radius: 2}],
        Connector:"StateMachine",
        HoverPaintStyle: {stroke: "#1e8151", strokeWidth: 2 },
        ConnectionOverlays: [
            [ "Arrow", {
                location: 1,
                id: "arrow",
                length: 14,
                foldback: 0.8
            } ],
            //[ "Label", { label: "teksti", id: "label", cssClass: "label" }]
        ],
        Container: "canvas"
    });

    instance.registerConnectionType("basic", { anchor:"Continuous", connector:"StateMachine" });

    window.jsp = instance;

    var canvas = document.getElementById("canvas");
    var rect = document.getElementById("rect");
    var round = document.getElementById("round");
    var elli = document.getElementById("elli");
    var windows = jsPlumb.getSelector(".window")

    instance.bind("click", function (c) {
        instance.deleteConnection(c);
        alert("moro");
    });

    instance.bind("connection", function (info) {
        //info.connection.getOverlay("label").setLabel("teksti");
        instance.repaintEverything();
    });

    jsPlumb.on(rect, "click", function(e) {
        newNode("rect");
    });
    jsPlumb.on(round, "click", function(e) {
        newNode("round");
    });
    jsPlumb.on(elli, "click", function(e) {
        newNode("elli");
    });


    var initNode = function(el) {
        $('[data-toggle="popover"]').popover(); 
        instance.draggable(el, {
            containment:true,
            filter:".ui-resizable-handle, .edit, .delete"
          });

        $( ".window" ).resizable({
            resize : function(event, ui) {
                var window = $(this)[0];
                instance.repaintEverything();
                $(this).find("a").popover('update');
            },
        });
        $( ".window" ).mouseup(function() {
            $(this).find("a").popover('update');
          });
        $( ".jtk-connector" ).click(function() {
            alert("hallooo :D");
          });
        $( ".delete" ).click(function() {
            var window = $(this).parent()[0];
            instance.remove(window);
          });
        $( ".windowText" ).dblclick(function() {
            var span = $(this)[0];
            var window = $(this).parent()[0];
            var text = $(this).text();
            var input = document.createElement("input");
            input.setAttribute("type", "text");
            input.classList.add("nodeInput");
            window.appendChild(input);
            input.focus();
            span.remove();
          });
          $(".window").keypress(function(e) {
            if(e.which == 13) {
                console.log("enter");
                if($("input").is(":focus")) {
                    var text = $(this).find("input")[0].value;
                    if (text.length > 0) {
                        console.log("on");
                        var span = document.createElement("span");
                        span.classList.add("windowText");
                        span.innerHTML = text;
                        $(this)[0].appendChild(span);
                        var textbox = $(this).find("input")[0].remove();
                        $( ".windowText" ).dblclick(function() {
                            var span = $(this)[0];
                            var window = $(this).parent()[0];
                            var text = $(this).text();
                            var input = document.createElement("input");
                            input.setAttribute("type", "text");
                            input.classList.add("nodeInput");
                            window.appendChild(input);
                            input.focus();
                            span.remove();
                          });
                    }
                }
            }
        });
        

        instance.makeSource(el, {
            filter: ".ep",
            anchor: "Continuous",
            connectorStyle: { stroke: "#5c96bc", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4 },
            connectionType:"basic",
            extract:{
                "action":"the-action"
            },
        });

        instance.makeTarget(el, {
            dropOptions: { hoverClass: "dragHover" },
            anchor: "Continuous",
            allowLoopback: true
        });

        instance.fire("jsPlumbDemoNodeAdded", el);
    };

    var newNode = function(shape) {
        var d = document.createElement("div");
        var ep = document.createElement("div");
        var edit = document.createElement("a");
        var editj = $(edit);
        editj.attr({
            "tabindex": "0",
            "role": "button",
            "data-toggle": "popover",
            "data-trigger": "focus",
            "title": "MORO!",
            "data-content": "<div class='delete'</div>",
            "data-placement": "bottom",
            "data-html": true
        });

        var remove = document.createElement("div");
        var span = document.createElement("span");
        span.classList.add("windowText");
        ep.classList.add("ep");
        edit.classList.add("edit");
        remove.classList.add("delete");
        var id = jsPlumbUtil.uuid();
        d.classList.add("window");
        if (shape == "rect") d.classList.add("rectangle");
        if (shape == "round") d.classList.add("round");
        if (shape == "elli") d.classList.add("ellipsis");
        d.id = id;
        span.innerHTML = "Double click";
        d.appendChild(ep);
        d.appendChild(edit);
        d.appendChild(remove);
        d.appendChild(span);
        instance.getContainer().appendChild(d);
        initNode(d);
        return d;
    };
    instance.batch(function () {
        for (var i = 0; i < windows.length; i++) {
            initNode(windows[i], true);
        }
    });

    jsPlumb.fire("jsPlumbDemoLoaded", instance);

});





