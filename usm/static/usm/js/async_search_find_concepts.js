// Copyright Sparta Consulting Oy 2018
function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

$(function searchConcept() {

    $('#searchconcept-input').keyup(function () {

        $.ajax({
            type: "POST",
            url: $("#hidden-search-url-projectid").val(),
            data: {
                'button': "searchproject-input",
                'page': getUrlParameter('page'),
                'search_text': $('#searchconcept-input').val(),
                'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()
            },
            success: conceptSearchSuccess,
            dataType: 'html'
        });
    });
});

function conceptSearchSuccess(data, textStatus, jqXHR) {
    $('#search-results-concept').html(data)
}