var findid = "";
var boxname = "";
var description = "";
var colorBackground;
var box;
var relation;
var relcolor;
var connection;
var colorText;
var shape;
var defaultColor = "#ffffff";
var textDefault = "#000000";
var nav_closed_byuser = false;
var nav_open = false;
var relation_open = false;
var attributes = {};
var selection_open = false;
var selection = [];

function rgb2hex(rgb) {          //Use this function to return hex value from rgb value
    rgb = rgb.match(/[0-9]+/g);
    return "#" +
        ("0" + parseInt(rgb[0], 10).toString(16)).slice(-2) +
        ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
        ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2);
}

window.addEventListener("load", startup, false);        //Change background color for a spesific box in the canvas
function startup() {
    colorBackground = document.querySelector("#change-color-btn");
    colorBackground.value = defaultColor;
    colorBackground.addEventListener("input", updateBgColor, false);
    colorBackground.select();
    colorText = document.querySelector("#change-textcolor-btn"); // Change textcolor for a spesific box in the canvas
    colorText.value = textDefault;
    colorText.addEventListener("input", updateTextColor, false);
    colorText.select();
}

/* Change shape for a spesific box in the canvas */

function updateShapeRound() {
    if (selection_open) {
        [].forEach.call(selection, function (node) {
            node.style.borderRadius = "15px";
        });
    }
    else {
        box.style.borderRadius = "15px";
    }
    setSavedState(false);
}

function updateShapeEllipsis() {
    if (selection_open) {
        [].forEach.call(selection, function (node) {
            node.style.borderRadius = "100%";
        });
    }
    else {
        box.style.borderRadius = "100%";
    }
    setSavedState(false);
}

function updateShapeBox() {
    if (selection_open) {
        [].forEach.call(selection, function (node) {
            node.style.borderRadius = "0px";
        });
    }
    else {
        box.style.borderRadius = "0px";
    }
    setSavedState(false);
}

function updateStyle(style) {
    switch (style) {
        case "solid": connection.setPaintStyle({
            stroke: relcolor, strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4, dashstyle: "none"
        });
            connection.setParameter("style", "solid");
            break;
        case "dashed": connection.setPaintStyle({
            stroke: relcolor, strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4, dashstyle: "4 2"
        });
            connection.setParameter("style", "dashed");
            break;
        case "pointed": connection.setPaintStyle({
            stroke: relcolor, strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4, dashstyle: "1 2"
        });
            connection.setParameter("style", "pointed");
            break;
    }
    setSavedState(false);
}
function updateRelColor(col) {
    switch (col) {
        case "blue":
            connection.setPaintStyle({
                stroke: "#3671d1"
            });
            relcolor = "#3671d1"
            break;
        case "green":
            connection.setPaintStyle({
                stroke: "#00a00d"
            });
            relcolor = "#00a00d";
            break;
        case "red":
            connection.setPaintStyle({
                stroke: "#e91d1d"
            });
            relcolor = "#e91d1d";
            break;
        case "black":
            connection.setPaintStyle({
                stroke: "#000000"
            });
            relcolor = "#000000";
            break;
    }
    setSavedState(false);
}

function updateBlueColor() {
    if (selection_open) {
        [].forEach.call(selection, function (node) {
            node.style.backgroundColor = "#80ddf2";
            node.style.color = "#000000";
        });
    }
    else {
        box.style.backgroundColor = "#80ddf2";
        box.style.color = "#000000";
    }
    setSavedState(false);
}

function updateGreenColor() {
    if (selection_open) {
        [].forEach.call(selection, function (node) {
            node.style.backgroundColor = "#87f2b9";
            node.style.color = "#000000";
        });
    }
    else {
        box.style.backgroundColor = "#87f2b9";
        box.style.color = "#000000";
    }
    setSavedState(false);
}
function updateOrangeColor() {
    if (selection_open) {
        [].forEach.call(selection, function (node) {
            node.style.backgroundColor = "#f4df89";
            node.style.color = "#000000";
        });
    }
    else {
        box.style.backgroundColor = "#f4df89";
        box.style.color = "#000000";
    }
    setSavedState(false);
}
function updatePinkColor() {
    if (selection_open) {
        [].forEach.call(selection, function (node) {
            node.style.backgroundColor = "#efa5d3";
            node.style.color = "#000000";
        });
    }
    else {
        box.style.backgroundColor = "#efa5d3";
        box.style.color = "#000000";
    }
    setSavedState(false);
}

function updateText(event) {
    document.getElementById("change-textcolor-btn").value = event.target.value;
}

function shadowBox() {
    var checkBox = document.getElementById("shadow-checkbox");
    if (checkBox.checked == true) {
        if (selection_open) {
            [].forEach.call(selection, function (node) {
                node.classList.add("boxshadow-checked");
            });
        }
        else {
            box.classList.add("boxshadow-checked");
        }

    }
    else {
        box.classList.remove("boxshadow-checked");
    }
    setSavedState(false);
}

function descriptionBox() {
    var textArea = document.getElementById("box-description");

    if (relation_open) {
        connection.setParameter("desc", textArea.value);
    }
    else {
        var text = box.querySelector('#textvalue');
        text.value = textArea.value;
        box.description = text.value;
    }
}

function addAttribute() {    // adds attribute to list when add attribute button is pressed
    var inputattributename = document.getElementById("attributename");
    var inputattributevalue = document.getElementById("attributevalue");
    if (attributes[findid] == null) {
        attributes[findid] = {};
    }
    attributes[findid][inputattributename.value] = inputattributevalue.value;
    inputattributename.value = "";
    inputattributevalue.value = "";
    updateAttributes();
    setSavedState(false);
}

function updateBgColor(event) {
    if (relation_open) {
        connection.setPaintStyle({
            stroke: event.target.value
        })
    }
    else if (selection_open) {
        [].forEach.call(selection, function (node) {
            node.style.backgroundColor = event.target.value;
        });
    }
    else {
        box.style.backgroundColor = event.target.value;
    }
    setSavedState(false);
}
function updateTextColor(event) {
    if (selection_open) {
        [].forEach.call(selection, function (node) {
            node.style.color = event.target.value;
        });
    }
    else {
        box.style.color = event.target.value;
    }
    setSavedState(false);
}

function openNav(e) {
    if (e != null) {
        nav_closed_byuser = false;
    }
    if (e != "rel") {
        box.classList.add("selectedWindow");
        //refreshPropertyMenu(box, "open");
    }
    nav_open = true;
    document.getElementById("property-closed").style.width = "0px";
    document.getElementById("property-menu").style.width = "250px";
}

function closeNav(e) {
    if (e != null) {
        nav_closed_byuser = true;
    }
    nav_open = false;
    document.getElementById("property-menu").style.width = "0px";
    document.getElementById("property-closed").style.width = "50px";
}

function updateAttributes() {                // Update attributes for boxes
    var attributelist = document.getElementById("attributes");
    while (attributelist.hasChildNodes()) {
        attributelist.removeChild(attributelist.childNodes[0]);
    }
    if (attributes[findid] == null) {
        attributes[findid] = {};
    }
    var keys = Object.keys(attributes[findid]);

    var arr = keys.length;
    for (var i = 0; i < arr; i++) {
        var tdn = document.createElement("td");
        var tdv = document.createElement("td");
        var button = document.createElement("button");
        keytoString = keys[i].toString();
        button.setAttribute("onclick", "rmvAttribute('" + keytoString + "')");
        button.setAttribute("class", "removeattributebutton");
        var tr = document.createElement("tr");
        document.getElementById("attributes").appendChild(tr);
        tdn.innerHTML = keys[i];
        tdv.innerHTML = attributes[findid][keys[i]];
        tr.appendChild(tdn);
        tr.appendChild(tdv);
        tr.appendChild(button);
    }
}

function rmvAttribute(key) {
    delete attributes[findid][key];
    updateAttributes();
}
//This function is triggered every time, when box is mousedowned in canvas
function refreshPropertyMenu(data, param) {
    relation_open = false;
    $(".relprop").hide();
    $(".nodeprop").show();
    if (Array.isArray(data)) {
        $("#box-description").hide();
        selection_open = true;
        selection = data;
        $("#boxlabel").text(data.length + " elements selected");
        if (!nav_closed_byuser) {
            openNav("rel");
        }
    }
    else {
        $("#box-description").show();
        selection_open = false;
        findid = data.id;
        boxname = $(data).find("span").text();

        box = document.getElementById(findid); /* Finds spesific box in the canvas with it's id */
        var checkBox = document.getElementById("shadow-checkbox");

        document.getElementById("change-color-btn").value = rgb2hex(box.style.backgroundColor);
        document.getElementById("change-textcolor-btn").value = rgb2hex(box.style.color);

        if (data.classList.contains("boxshadow-checked")) {
            checkBox.checked = true;
        }
        else {
            checkBox.checked = false;
        }

        var label = document.getElementById("boxlabel");
        var desctext = document.getElementById("box-description");
        var desc = box.querySelector("#textvalue").value;
        label.textContent = boxname;
        desctext.value = desc;
        if (!nav_closed_byuser && param == null) {
            openNav();
        }
        updateAttributes();
        return findid, boxname, box;
    }

}

function refreshRelationMenu(conn) {
    selection_open = false;
    relation_open = true;
    $("#box-description").show();
    $(".relprop").show();
    connection = conn;
    var textArea = document.getElementById("box-description");
    if (conn.getParameter("desc") != null) {
        textArea.value = conn.getParameter("desc");
    }
    else {
        textArea.value = "";
    }
    $("#boxlabel").html("<b>Relation</b><br><u>Source</u>: " + $(conn.source).find("span").text() + "<br>" + "<u>Target</u>: " + $(conn.target).find("span").text());
    var rgb = $(conn.connector.path).css("stroke");
    relcolor = rgb2hex(rgb);
    relation = conn.connector.path;
    document.getElementById("change-color-btn").value = relcolor;
    $(".nodeprop").hide();
    if (!nav_closed_byuser) {
        openNav("rel");
    }
}

function hideContext() {
    var contextbox = $("#nodecontext");
    contextbox.hide();
}