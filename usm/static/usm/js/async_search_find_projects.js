// Copyright Sparta Consulting Oy 2018
function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

$(function searchProject() {

    $('#searchproject-input').keyup(function () {

        $.ajax({
            type: "POST",
            url: $("#hidden-ajax-url").val(),
            data: {
                'button': "ajax-search",
                'page': getUrlParameter('page'),
                'search_text': $('#searchproject-input').val(),
                'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()
            },
            success: projectSearchSuccess,
            dataType: 'html'
        });
    });
});

function projectSearchSuccess(data, textStatus, jqXHR) {
    $('#search-results-project').html(data)
}