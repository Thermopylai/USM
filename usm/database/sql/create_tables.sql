create table user (
    id INTEGER PRIMARY KEY,
    fullname varchar(100) not null,
    email varchar(100) not null,
    passwd varchar(200) not null,
    UUID varchar(100) not null,

    CONSTRAINT uq_uuid UNIQUE (UUID)
);

create table concept (
    id INTEGER PRIMARY KEY,
    name varchar(100) not null,
    UUID varchar(100) not null,
    data varchar(100),

    
    CONSTRAINT uq_uuid UNIQUE (UUID)
);

create table permission (
    id INTEGER PRIMARY KEY,
    name varchar(100) not null,
    UUID varchar(100) not null,

    CONSTRAINT uq_uuid UNIQUE (UUID)
);

create table role (
    id INTEGER PRIMARY KEY,
    name varchar(100) not null,
    UUID varchar(100) not null,

    CONSTRAINT uq_uuid UNIQUE (UUID)
);

create table project (
    id INTEGER PRIMARY KEY,
    name varchar(100) not null,
    UUID varchar(100) unique not null,
    createtime date not null,
    created_by int not null,

    CONSTRAINT uq_uuid UNIQUE (UUID),
    FOREIGN KEY(created_by) REFERENCES user(id)
);

create table project_concept (
    id INTEGER PRIMARY KEY,
    project_id int not null,
    concept_id int not null,

    FOREIGN KEY(project_id) REFERENCES project(id),
    FOREIGN KEY(concept_id) REFERENCES concept(id)
);

create table role_permission (
    id INTEGER PRIMARY KEY,
    role_id int not null,
    permission_id int not null,

    FOREIGN KEY(role_id) REFERENCES role(id),
    FOREIGN KEY(permission_id) REFERENCES permission(id)
);

create table member (
    id INTEGER PRIMARY KEY,
    user_id int not null,
    project_id int not null,
    role_id int not null,

    FOREIGN KEY(user_id) REFERENCES user(id),
    FOREIGN KEY(project_id) REFERENCES project(id),
    FOREIGN KEY(role_id) REFERENCES role(id)
);