insert into user (fullname, email, passwd, UUID)
values ('Veeti', 'veeti', 'qwerty', '123');

insert into concept (name, UUID)
values ('Nokian saapas tuotanto', '911');

insert into role (name, UUID)
values ('admin', '321');

insert into permission (name, UUID)
values ('veeti', 'asdfg');

insert into project (name, UUID, createtime, created_by)
values ('Nokia prokkis', 'afds', '2018-01-12', (select id from user where fullname = 'Veeti'));

insert into project_concept (project_id, concept_id)
values ((select id from project where name = 'Nokia prokkis'), (select id from concept where name = 'Nokian saapas tuotanto'));

insert into role_permission (role_id, permission_id)
values ((select id from role where name = 'admin'), (select id from permission where name = 'veeti'));

insert into member (user_id, project_id, role_id)
values ((select id from user where fullname = 'Veeti'), (select id from project where name = 'Nokia prokkis'), (select id from role where name = 'admin'));