*** Variables ***
${COMMENT}          Nice work, looks great! Can't wait to this finished.
${CONCEPT RENAMED}  Space Y

*** Keywords ***


## PRE-SETS


Open Browser To Conceptpage
    Open Browser To Startpage
    Login User Valid
    Click Element                   project-table-id
    Wait Until Element Is Visivble  //*[@id="search-results-concept"]/table/tbody/tr
    Click Element                   //*[@id="search-results-concept"]/table/tbody/tr

Create Headless Browser Conceptpage
    Start Virtual Display           1920    1080
    Open Browser                    ${STARTPAGE URL}    ${BROWSER}
    Set Window Size                 1920    1080
    Login User Valid
    Wait Until Element Is Visible   project-table-id
    Click Element                   project-table-id
    Wait Until Element Is Visible   //*[@id="search-results-concept"]/table/tbody/tr
    Click Element                   //*[@id="search-results-concept"]/table/tbody/tr


## CONCEPT


Add Comment And Delete Comment
    Wait Until Element Is Visible   id_message
    Input Text                      id_message      ${COMMENT}
    Wait Until Element Is Visible   send-comment-btn
    Click Element                   send-comment-btn
    Page Should Contain Button      delete-comment-btn
    Wait Until Element Is Visible   delete-comment-btn
    Click Element                   delete-comment-btn
    Handle Alert
    Page Should Not Contain         ${COMMENT}

Rename Concept
    Wait Until Element is Visible   concept-settings-btn
    Click Element                   concept-settings-btn
    Wait Until Element Is Visible   rename-concept-textfield
    Input Text                      rename-concept-textfield    ${CONCEPT RENAMED}
    Wait Until Element Is Visible   rename-concept-btn
    Click Element                   rename-concept-btn
    Page Should Contain             ${CONCEPT RENAMED}

Delete Concept
    Wait Until Element is Visible   concept-settings-btn
    Click Element                   concept-settings-btn
    Wait Until Element is Visible   delete-concept-btn
    Click Element                   delete-concept-btn
    Handle Alert
    Page Should Not Contain         ${CONCEPT RENAMED}




