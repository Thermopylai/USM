*** Variables ***
${USERNAME}             mrrobot
${PASSWORD}             pass1234
${PASSWORD INVALID}     pass12345
${PASSWORD NEW}         news1234
${PASSWORD SHORT}       pass123
${EMAIL}                robot@robotmail.com
${EMAIL INVALID}        robot.robotmail.com
${REGISTER MODAL}       SRModal

*** Keywords ***


##PRE-SETS


Create Headless Browser Profilepage
    Start Virtual Display       1920    1080
    Open Browser                ${STARTPAGE URL}    ${BROWSER}
    Set Window Size             1920    1080
    Login User Valid
    Wait Until Element Is Visible   profile-dropdown-btn
    Click Element                   profile-dropdown-btn
    Wait Until Element Is Visible   profile-btn
    Click Element                   profile-btn

Login User Changed
    Wait Until Element Is Visible   SRModal-btn
    Click Element                   SRModal-btn
    Wait Until Element Is Visible   signin-tab-btn
    Click Element                   signin-tab-btn
    Wait Until Element Is Visible   id_username
    Input Text                      id_username   ${USERNAME}
    Wait Until Element Is Visible   id_password
    Input Text                      id_password   ${PASSWORD NEW}
    Wait Until Element Is Visible   login_btn
    Click Element                   login_btn
    Location Should Be              ${MAINPAGE URL}


## PASSWORD CHANGES

Open Projectpage
    Location Should Be      ${PROFILEPAGE URL}

Change Password Wrong Old Passworld
    Wait Until Element Is Visible       id_old_password
    Input Text                          id_old_password     ${PASSWORD INVALID}
    Wait Until Element Is Visible       id_new_password1
    Input Text                          id_new_password1    ${PASSWORD NEW}
    Wait Until Element Is Visible       id_new_password2
    Input Text                          id_new_password2    ${PASSWORD NEW}
    Wait Until Element Is Visible       changepassword_btn
    Click Element                       changepassword_btn
    Page Should Contain                 Your old password was entered incorrectly. Please enter it again.

Change Password New Passwords Dont Match
    Wait Until Element Is Visible       id_old_password
    Input Text                          id_old_password     ${PASSWORD}
    Wait Until Element Is Visible       id_new_password1
    Input Text                          id_new_password1    ${PASSWORD NEW}
    Wait Until Element Is Visible       id_new_password2
    Input Text                          id_new_password2    ${PASSWORD}
    Wait Until Element Is Visible       changepassword_btn
    Click Element                       changepassword_btn
    Page Should Contain                 The two password fields didn't match.

Change Password New Passwords Too Short
    Wait Until Element Is Visible       id_old_password
    Input Text                          id_old_password     ${PASSWORD}
    Wait Until Element Is Visible       id_new_password1
    Input Text                          id_new_password1    ${PASSWORD SHORT}
    Wait Until Element Is Visible       id_new_password2
    Input Text                          id_new_password2    ${PASSWORD SHORT}
    Wait Until Element Is Visible       changepassword_btn
    Click Element                       changepassword_btn
    Page Should Contain                 This password is too short. It must contain at least 8 characters.

Change Password Valid
    Wait Until Element Is Visible       id_old_password
    Input Text                          id_old_password     ${PASSWORD}
    Wait Until Element Is Visible       id_new_password1
    Input Text                          id_new_password1    ${PASSWORD NEW}
    Wait Until Element Is Visible       id_new_password2
    Input Text                          id_new_password2    ${PASSWORD NEW}
    Wait Until Element Is Visible       changepassword_btn
    Click Element                       changepassword_btn
    Login User Changed