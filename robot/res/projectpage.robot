*** Variables ***
${CONCEPT NAME}         SpaceX
${MEMBER NAME}          admin

*** Keywords ***


## PRE-SETS

Create Headless Browser Projectpage
    Start Virtual Display    1920    1080
    Open Browser    ${STARTPAGE URL}    ${BROWSER}
    Set Window Size    1920    1080
    Login User Valid

Delete Created Project
    Open Browser To Startpage
    Delete Project
    Close Browser


## CONCEPTS

Create Concept
    Create Project
    Wait Until Element Is Visible   //*[@id="home"]/div[1]/div/button
    Click Element                   //*[@id="home"]/div[1]/div/button
    Wait Until Element Is Visible   id_name
    Input Text                      id_name     ${CONCEPT NAME}
    Wait Until Element Is Visible   createconcept_btn
    Click Element                   createconcept_btn
    Wait Until Element Is Visible   //*[@id="search-results-concept"]/table/tbody/tr/td[1]
    Element Text Should Be          //*[@id="search-results-concept"]/table/tbody/tr/td[1]    ${CONCEPT NAME}
    Click Element                   //*[@id="search-results-concept"]/table/tbody/tr/td[1]

Invite Member
    Wait Until Element Is Visible   //*[@id="project-table-id"]
    Click Element                   //*[@id="project-table-id"]
    Wait Until Element Is Visible   profile-tab-members
    Click Element                   profile-tab-members
    Wait Until Element Is Visible   invite-member-btn
    Click Element                   invite-member-btn
    Wait Until Element Is Visible   searchmember-input
    Input Text                      searchmember-input     ${MEMBER NAME}
    Wait Until Element Is Visible   add-member-btn
    Click Element                   add-member-btn
    Wait Until Element Is Visible   send-invites-btn
    Click Element                   send-invites-btn
    Wait Until Element Is Visible   profile-tab-members
    Click Element                   profile-tab-members
    Page Should Contain             ${MEMBER NAME}
