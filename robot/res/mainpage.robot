*** Variables ***
${PROJECT NAME}         Tesla
${PROJECT NAME CHANGE}  Microsoft

*** Keywords ***


## PRE-SETS


Open Browser To Mainpage
    Open Browser To Startpage
    Login User Valid

Create Headless Browser Mainpage
    Start Virtual Display    1920    1080
    Open Browser    ${STARTPAGE URL}    ${BROWSER}
    Set Window Size    1920    1080
    Login User Valid


## PROJECT


Navigation correct
    Location Should Be      ${MAINPAGE URL}

Create Project
    Wait Until Element Is Visible   create-project-btn
    Click Element                   create-project-btn
    Wait Until Element Is Visible   id_name
    Input Text                      id_name     ${PROJECT NAME}
    Wait Until Element Is Visible   createproject_btn
    Click Element                   createproject_btn
    Wait Until Element Is Visible   //*[@id="project-table-id"]/td[1]
    Element Text Should Be          //*[@id="project-table-id"]/td[1]     ${PROJECT NAME}
    Click Element                   //*[@id="project-table-id"]/td[1]

Rename Project
    Wait Until Element Is Visible   project-table-id
    Click Element                   project-table-id
    Wait Until Element Is Visible   profile-tab-settings
    Click Element                   profile-tab-settings
    Wait Until Element Is Visible   rename-project-textfield
    Click Element                   rename-project-textfield
    Input Text                      rename-project-textfield    ${PROJECT NAME CHANGE}
    Wait Until Element Is Visible   rename-project-btn
    Click Element                   rename-project-btn
    Wait Until Element Is Visible   //*[@id="breadcrumb-project-link"]/a
    Click Element                   //*[@id="breadcrumb-project-link"]/a
    Wait Until Element Is Visible   //*[@id="project-table-id"]/td[1]
    Element Text Should Be          //*[@id="project-table-id"]/td[1]     ${PROJECT NAME CHANGE}

Delete Project
    Wait Until Element Is Visible   project-table-id
    Click Element                   project-table-id
    Wait Until Element Is Visible   profile-tab-settings
    Click Element                   profile-tab-settings
    Wait Until Element Is Visible   delete-project-btn
    Click Element                   delete-project-btn
    Handle Alert
    Wait Until Element Is Visible   breadcrumb-project-link
    Click Element                   breadcrumb-project-link
    Page Should Not Contain         ${PROJECT NAME CHANGE}





