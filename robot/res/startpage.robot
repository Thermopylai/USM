*** Variables ***
${USERNAME}             mrrobot
${USERNAME INVALID}     mrrobott
${USERNAME TAKEN}       admin
${PASSWORD}             pass1234
${PASSWORD INVALID}     pass12345
${PASSWORD SHORT}       pass123
${EMAIL}                robot@robotmail.com
${EMAIL INVALID}        robot.robotmail.com
${REGISTER MODAL}       SRModal

*** Keywords ***


##PRE-SETS
Create Headless Browser Startpage
    Start Virtual Display    1920    1080
    Open Browser    ${STARTPAGE URL}    ${BROWSER}
    Set Window Size    1920    1080

Startpage Should Be Open
    Location Should Be      ${STARTPAGE URL}


## REGISTER


Register User Passwords Dont Match
    Wait Until Element Is Visible   SRModal-btn
    Click Element                   SRModal-btn
    Wait Until Element Is Visible   register-tab-btn
    Click Element                   register-tab-btn
    Input Text      id_username_register    ${USERNAME}
    Input Text      id_password1            ${PASSWORD}
    Input Text      id_password2            ${PASSWORD INVALID}
    Input Text      id_email                ${EMAIL}
    Input Text      id_firstname            Mister
    Input Text      id_lastname             Robato
    Click Element                           register-btn
    Wait Until Element Is Visible           ${REGISTER MODAL}
    Element Text Should Be                  //*[@id="register"]/form/div/div[3]/span/ul/li  The two password fields didn't match.

Register User Passwords Too Short
    Wait Until Element Is Visible   SRModal-btn
    Click Element   SRModal-btn
    Wait Until Element Is Visible   register-tab-btn
    Click Element   register-tab-btn
    Input Text      id_username_register    ${USERNAME}
    Input Text      id_password1            ${PASSWORD SHORT}
    Input Text      id_password2            ${PASSWORD SHORT}
    Input Text      id_email                ${EMAIL}
    Input Text      id_firstname            Mister
    Input Text      id_lastname             Robato
    Click Element   register-btn
    Wait Until Element Is Visible          ${REGISTER MODAL}
    Element Text Should Be                  //*[@id="register"]/form/div/div[3]/span/ul/li  This password is too short. It must contain at least 8 characters.

Register User Password Too Similiar
    Wait Until Element Is Visible   SRModal-btn
    Click Element   SRModal-btn
    Wait Until Element Is Visible   register-tab-btn
    Click Element   register-tab-btn
    Input Text      id_username_register    ${USERNAME}
    Input Text      id_password1            ${USERNAME INVALID}
    Input Text      id_password2            ${USERNAME INVALID}
    Input Text      id_email                ${EMAIL}
    Input Text      id_firstname            Mister
    Input Text      id_lastname             Robato
    Click Element   register-btn
    Wait Until Element Is Visible          ${REGISTER MODAL}
    Element Text Should Be                  //*[@id="register"]/form/div/div[3]/span/ul/li  The password is too similar to the username.

Register User Username Already Exists
    Wait Until Element Is Visible   SRModal-btn
    Click Element   SRModal-btn
    Wait Until Element Is Visible   register-tab-btn
    Click Element   register-tab-btn
    Input Text      id_username_register    ${USERNAME TAKEN}
    Input Text      id_password1            ${PASSWORD}
    Input Text      id_password2            ${PASSWORD}
    Input Text      id_email                ${EMAIL}
    Input Text      id_firstname            Mister
    Input Text      id_lastname             Robato
    Click Element   register-btn
    Wait Until Element Is Visible          ${REGISTER MODAL}
    Element Text Should Be                  //*[@id="register"]/form/div/div[1]/span/ul/li  A user with that username already exists.

Register User Invalid Email
    Wait Until Element Is Visible   SRModal-btn
    Click Element   SRModal-btn
    Wait Until Element Is Visible   register-tab-btn
    Click Element   register-tab-btn
    Input Text      id_username_register    ${USERNAME}
    Input Text      id_password1            ${PASSWORD}
    Input Text      id_password2            ${PASSWORD}
    Input Text      id_email                ${EMAIL INVALID}
    Input Text      id_firstname            Mister
    Input Text      id_lastname             Robato
    Click Element   register-btn
    Wait Until Element Is Visible          ${REGISTER MODAL}
    Element Text Should Be                  //*[@id="register"]/form/div/div[4]/span/ul/li  Enter a valid email address.

Register User Succesfull
    Wait Until Element Is Visible   SRModal-btn
    Click Element   SRModal-btn
    Wait Until Element Is Visible   register-tab-btn
    Click Element   register-tab-btn
    Input Text      id_username_register    ${USERNAME}
    Input Text      id_password1            ${PASSWORD}
    Input Text      id_password2            ${PASSWORD}
    Input Text      id_email                ${EMAIL}
    Input Text      id_firstname            Mister
    Input Text      id_lastname             Robato
    Click Element   register-btn
    Location Should Be  ${STARTPAGE URL}


## LOGIN


Login User Valid
    Wait Until Element Is Visible   SRModal-btn
    Click Element   SRModal-btn
    Wait Until Element Is Visible   signin-tab-btn
    Click Element   signin-tab-btn
    Input Text      id_username   ${USERNAME}
    Input Text      id_password   ${PASSWORD}
    Click Element   login_btn
    Location Should Be  ${MAINPAGE URL}

Login User Invalid Password
    Wait Until Element Is Visible   SRModal-btn
    Click Element   SRModal-btn
    Wait Until Element Is Visible   signin-tab-btn
    Click Element   signin-tab-btn
    Input Text      id_username   ${USERNAME}
    Input Text      id_password   ${PASSWORD INVALID}
    Click Element   login_btn
    Location Should Be  ${STARTPAGE URL}
    Page Should Contain     Incorrect username or password

Login User Invalid Username
    Wait Until Element Is Visible   SRModal-btn
    Click Element   SRModal-btn
    Wait Until Element Is Visible   signin-tab-btn
    Click Element   signin-tab-btn
    Input Text      id_username   ${USERNAME INVALID}
    Input Text      id_password   ${PASSWORD}
    Click Element   login_btn
    Location Should Be  ${STARTPAGE URL}
    Page Should Contain     Incorrect username or password

Login User Invalid Username And Password
    Wait Until Element Is Visible   SRModal-btn
    Click Element   SRModal-btn
    Wait Until Element Is Visible   signin-tab-btn
    Click Element   signin-tab-btn
    Input Text      id_username   ${USERNAME INVALID}
    Input Text      id_password   ${PASSWORD INVALID}
    Click Element   login_btn
    Location Should Be  ${STARTPAGE URL}
    Page Should Contain     Incorrect username or password

Logout User
    Location Should be  ${MAINPAGE URL}
    Wait Until Element Is Visible   profile-dropdown-btn
    Click Element       profile-dropdown-btn
    Wait Until Element is Visible   logout-btn
    Click Element       logout-btn
    Location Should Be  ${STARTPAGE URL}







