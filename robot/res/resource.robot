*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           XvfbRobot
Resource          startpage.robot
Resource          mainpage.robot
Resource          projectpage.robot
Resource          conceptpage.robot
Resource          concept-toolpage.robot
Resource          profilepage.robot

*** Variables ***
${SERVER}                   localhost:8000
${BROWSER}                  Chrome
${DELAY}                    0.2
${STARTPAGE URL}            http://${SERVER}/
${MAINPAGE URL}             http://${SERVER}/main/
${PROJECTPAGE URL}
${CONCEPTPAGE URL}
${CONCEPT-TOOLPAGE URL}
${PROFILEPAGE URL}          http://${SERVER}/profile/