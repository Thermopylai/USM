*** Settings ***
Documentation     Test for renaming and deleting a concept
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../../res/resource.robot
Suite Setup       Create Headless Browser Conceptpage
Suite Teardown    Close Browser

*** Test Cases ***

Rename Concept
    Rename Concept

Delete Concept
    Delete Concept