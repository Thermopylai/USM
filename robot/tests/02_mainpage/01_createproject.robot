*** Settings ***
Documentation     Test to create projects and modify them
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../../res/resource.robot
Test Setup       Create Headless Browser Mainpage
Test Teardown    Close Browser

*** Test Cases ***

Open Mainpage
    Navigation correct

Create Project
    Create Project

Rename Project
    Rename Project

Delete Project
    Delete Project