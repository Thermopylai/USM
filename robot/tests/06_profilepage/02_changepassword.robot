*** Settings ***
Documentation     Test to change password in profile settings
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../../res/resource.robot
Suite Setup       Create Headless Browser Profilepage
Suite Teardown    Close Browser

*** Test Cases ***

Wrong Old Password
    Change Password Wrong Old Passworld

New Passwords Dont Match
    Change Password New Passwords Dont Match

New Password Too Short
    Change Password New Passwords Too Short

Change Password
    Change Password Valid