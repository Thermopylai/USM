*** Settings ***
Documentation     Test for user register
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../../res/resource.robot
Test Setup       Create Headless Browser Startpage
Test Teardown    Close Browser

*** Test Cases ***
Passwords Dont Match
    Register User Passwords Dont Match

Password Too Short
    Register User Passwords Too Short

Password Too Similiar
    Register User Password Too Similiar

Username Already Exists
    Register User Username Already Exists

Invalid Email
    Register User Invalid Email

Register Succesfull
    Register User Succesfull







