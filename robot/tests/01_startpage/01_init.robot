*** Settings ***
Documentation     Test navigation to startpage
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../../res/resource.robot
Suite Setup       Create Headless Browser Startpage
Suite Teardown    Close Browser

*** Test Cases ***
Navigate to Startpage
    Startpage Should Be Open