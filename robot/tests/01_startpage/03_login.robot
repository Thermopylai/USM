*** Settings ***
Documentation     Test for user login
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../../res/resource.robot
Test Setup       Create Headless Browser Startpage
Test Teardown    Close Browser

*** Test Cases ***
Login Invalid Username
    Login User Invalid Username

Login Invalid Password
    Login User Invalid Password

Login Invalid Username And Password
    Login User Invalid Username And Password

Login User Valid
    Login User Valid

Logout User
    Login User Valid
    Logout User