jsPlumb.ready(function () {

      var instance = jsPlumb.getInstance({
        Endpoint: ["Dot", {radius: 4}],
        Connector:"Straight",
        PaintStyle: { stroke: "#3671d1", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4 },
        HoverPaintStyle: {stroke: "#000000", strokeWidth: 2 },
        ConnectionOverlays: [
            [ "Arrow", {
                location: 1,
                id: "arrow",
                length: 14,
                foldback: 1
            } ]
        ],
        Container: "canvas"
    });
    var textConnector = {
        anchor:"Continuous",
        connector:"Straight",
        overlays: [
            [ "Label", { label: "Click to edit", id: "label", cssClass: "aLabel" }]
        ]
    };
        
    instance.registerConnectionType("regularArrow", { anchor:"Continuous", connector:"Straight" });
    instance.registerConnectionType("textArrow", textConnector);

    window.jsp = instance;

    var canvas = document.getElementById("canvas");
    var rect = document.getElementById("rect");
    var round = document.getElementById("round");
    var elli = document.getElementById("elli");
    var nodes = [];
    var connection = "regularArrow";
    var shape = "rectangle";
    var selectedWindow = "";
    var data = {};

    instance.bind("click", function (event) {
        var label = event.getOverlay("label").getElement();
        var text = label.innerHTML;
        label.innerHTML = "";
        var input = document.createElement("input");
        input.setAttribute("type", "text");
        input.classList.add("nodeInput");
        label.appendChild(input);
        input.value = text;
        input.focus();
        input.select();
        input.onfocusout = function() {
            var value = input.value;
            input.remove();
            event.getOverlay("label").setLabel(value);
        }
        input.onkeydown = function(e) {
            if (e.keyCode == 13) {
                input.blur();
            }
        }
    });
    instance.bind("endpointClick", function (ep) {
        var connection = ep.connections[0];
        if (connection.source == ep.element) {
            //source point clicked
            if (connection.getOverlay("sourceArrow") == null) {
                connection.addOverlay([ "Arrow", {location: 0, direction: -1, id: "sourceArrow", length: 14, foldback: 1 }]);
            }
            else {
                connection.removeOverlay("sourceArrow");
            }
        }
        else if (connection.target == ep.element) {
            //target point clicked
            if (connection.getOverlay("arrow") == null) {
                connection.addOverlay([ "Arrow", {location: 1, id: "arrow", length: 14, foldback: 1 }]);
            }
            else {
                connection.removeOverlay("arrow");
            }

        }
    });

    instance.bind("connection", function (info) {
        instance.repaintEverything();
    });
    instance.bind("endpointClick", function (ep) {
        var connection = ep.connections[0];
        if (connection.source == ep.element) {
            //source point clicked
            if (connection.getOverlay("sourceArrow") == null) {
                connection.addOverlay([ "Arrow", {location: 0, direction: -1, id: "sourceArrow", length: 14, foldback: 1 }]);
            }
            else {
                connection.removeOverlay("sourceArrow");
            }
        }
        else if (connection.target == ep.element) {
            //target point clicked
            if (connection.getOverlay("arrow") == null) {
                connection.addOverlay([ "Arrow", {location: 1, id: "arrow", length: 14, foldback: 1 }]);
            }
            else {
                connection.removeOverlay("arrow");
            }

        }
    });
    $("#canvas:not(.window)").dblclick(function(e) {
        if (e.target.id == "canvas") {
            newNode(shape, e.offsetX, e.offsetY);
        }
    });

    $( ".shape" ).click(function(e) {
        var id = e.target.id;
        if (id != shape) {
            var shapes = document.querySelectorAll(".shape");
            [].forEach.call(shapes, function(el) {
                el.classList.remove("selected");
            });
            e.target.classList.add("selected");
            shape = id;
        }
        
    });

    $( "#saveButton" ).click(function(e) {
        //todo: blur inputs when saving, or redo so that on no input unfocu
        //      they are reverted to span elements
        var data = {};
        var windowArray = $(".window");
        data.nodes = {};
        for (i=0;i<windowArray.length;i++) {
            data.nodes[i] = {};
            if (windowArray[i].classList.contains("rectangle")) {
                data.nodes[i].shape = "rectangle";
            }
            if (windowArray[i].classList.contains("round")) {
                data.nodes[i].shape = "round";
            }
            if (windowArray[i].classList.contains("ellipsis")) {
                data.nodes[i].shape = "ellipsis";
            }
            data.nodes[i].id = windowArray[i].id;
            data.nodes[i].left = windowArray[i].style.left;
            data.nodes[i].top = windowArray[i].style.top;
            data.nodes[i].width = windowArray[i].offsetWidth;
            data.nodes[i].height = windowArray[i].offsetHeight;
            var object = jQuery(windowArray[i]);
            var text = object.find("span").text();
            data.nodes[i].text = text;
        }
        data.connections = {};
        var connectionsArray = instance.getAllConnections();
        for (i=0;i<connectionsArray.length;i++) {
            data.connections[i] = {overlays: {}, source:connectionsArray[i].source.id, target:connectionsArray[i].target.id};
            var overlaysArray = connectionsArray[i].getOverlays();
            var thisConnection = data.connections[i];
            for (var key in overlaysArray) {
                if (overlaysArray.hasOwnProperty(key)) {
                    if (key == "label") {
                        thisConnection.overlays.label = overlaysArray[key].label;
                    }
                    if (key == 'arrow') {
                        thisConnection.overlays.arrow = 1;
                    }
                    if (key == 'sourceArrow') {
                        thisConnection.overlays.sourceArrow = 1;
                    }
                }
            }
        }
        for (i=0; i<nodes.length;i++) {
            instance.remove(nodes[i]);
        }
        //json saved data
        console.log(JSON.stringify(data));
        console.log(data);
    });
    $( "#loadButton" ).click(function(e) {
        if (Object.keys(data).length === 0) {
            //nothing to load
        }
        else {
            for (var key in data.nodes) {
                var thisNode = data.nodes[key];
                newNode(thisNode.shape, thisNode.left, thisNode.top, [thisNode.id, thisNode.text, thisNode.height, thisNode.width]);
            }
            for (var key in data.connections) {
                var thisConn = data.connections[key];
                console.log(thisConn);
                var conn = instance.connect({
                    source: thisConn.source,
                    target: thisConn.target,
                    anchor: "Continuous",
                    type: "regularArrow"
                });
                if ('label' in thisConn.overlays) {
                    conn.addOverlay([ "Label", { label: thisConn.overlays.label, id: "label", cssClass: "aLabel" }]);
                }
            }

        }

    });
    $( ".arrowImg" ).click(function(e) {
        var id = e.target.id;
        console.log(e.target);
        if (id != connection){
            var arrows = document.querySelectorAll(".arrowImg");
            [].forEach.call(arrows, function(el) {
                el.classList.remove("selected");
            });
            e.target.classList.add("selected");
            if (connection == "regularArrow") {
                connection = "textArrow";
            }
            else {
                connection = "regularArrow";
            }
            for (i=0; i<nodes.length;i++) {
                instance.unmakeSource(nodes[i]);
                instance.makeSource(nodes[i], {
                    filter: ".ep",
                    anchor: "Continuous",
                    connectorStyle: { stroke: "#3671d1", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4 },
                    connectionType:connection,
                    extract:{
                        "action":"the-action"
                    }
                });
                instance.makeTarget(nodes[i], {
                    dropOptions: { hoverClass: "dragHover" },
                    anchor: "Continuous",
                    allowLoopback: true
                });
            }
        }
        
      });


    var initNode = function(el, isNew) {
        $( ".window" ).click(function() {
            for (i=0; i<nodes.length;i++) {
                nodes[i].classList.remove("selectedWindow");
            }
            selectedWindow = $(this)[0];
            selectedWindow.classList.add("selectedWindow");
          });
        $( document ).keydown(function(e) {
            if ($(e.target).closest("input")[0]) {
                return;
            }
            if (e.which == 46 && selectedWindow != "") {
                $("#deleteModal").modal("show");
                $('#deleteModal').on('shown.bs.modal', function (e) {
                $("#deleteButton").focus();
              })
                $("#deleteButton").click(function() {
                instance.remove(selectedWindow);
            });
            }
          });
        $('[data-toggle="popover"]').popover(); 
        instance.draggable(el, {
            containment:true,
            filter:".ui-resizable-handle, .edit, .delete"
          });
        
        $( ".window" ).resizable({
            resize : function(event, ui) {
                var window = $(this)[0];
                instance.repaintEverything();
                $(this).find("a").popover('update');
            },
        });

        $( ".windowText" ).dblclick(function() {
            var span = $(this)[0];
            var window = $(this).parent()[0];
            var text = $(this).text();
            var input = document.createElement("input");
            input.setAttribute("type", "text");
            input.classList.add("nodeInput");
            input.value = text;
            window.appendChild(input);
            input.focus();
            input.select();
            input.onfocusout = function() {
                var span = document.createElement("span");
                span.classList.add("windowText");
                span.innerHTML = input.value;
                window.appendChild(span);
                input.remove();
                initNode(window, false);
            }
            input.onkeydown = function(e) {
                if (e.keyCode == 13) {
                    input.blur();
                }
            }
            span.remove();
          });

          /*$(".window").keypress(function(e) {
            if(e.which == 13) {
                if($("input").is(":focus")) {
                    var text = $(this).find("input")[0].value;
                    if (text.length > 0) {
                        var span = document.createElement("span");
                        span.classList.add("windowText");
                        span.innerHTML = text;
                        $(this)[0].appendChild(span);
                        var textbox = $(this).find("input")[0].remove();
                        $( ".windowText" ).dblclick(function() {
                            var span = $(this)[0];
                            var window = $(this).parent()[0];
                            var text = $(this).text();
                            var input = document.createElement("input");
                            input.setAttribute("type", "text");
                            input.classList.add("nodeInput");
                            input.value = text;
                            window.appendChild(input);
                            input.focus();
                            input.select();
                            span.remove();
                          });
                    }
                }
            }
        });*/
        if (isNew) {
            instance.makeSource(el, {
                filter: ".ep",
                anchor: "Continuous",
                connectorStyle: { stroke: "#3671d1", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4 },
                connectionType:connection,
                extract:{
                    "action":"the-action"
                }
            });
    
            instance.makeTarget(el, {
                dropOptions: { hoverClass: "dragHover" },
                anchor: "Continuous",
                allowLoopback: true
            });
        }
        nodes.push(el);
        instance.fire("jsPlumbNodeAdded", el);
    };

    var newNode = function(shape, x, y, load) {
        var d = document.createElement("div");
        var ep = document.createElement("div");

        //editbutton - popover container

        /*var edit = document.createElement("a");
        var editj = $(edit);
        editj.attr({
            "tabindex": "0",
            "role": "button",
            "data-toggle": "popover",
            "data-trigger": "focus",
            "title": "MORO!",
            "data-content": "<div class='delete'</div>",
            "data-placement": "bottom",
            "data-html": true
        });*/
        var remove = document.createElement("div");
        var span = document.createElement("span");
        span.classList.add("windowText");
        ep.classList.add("ep");
        //edit.classList.add("edit");
        //remove.classList.add("delete");
        var id = jsPlumbUtil.uuid();
        d.classList.add("window");
        if (shape == "rectangle") d.classList.add("rectangle");
        if (shape == "round") d.classList.add("round");
        if (shape == "ellipsis") d.classList.add("ellipsis");
 
        d.appendChild(ep);
        //d.appendChild(edit);
        //d.appendChild(remove);
        var img = document.createElement("img");
        img.src = "../images/arrow.png";
        img.width = "30px";
        img.height = "15px";
        ep.appendChild(img);
        d.appendChild(span);
        if (load == null) {
            span.innerHTML = "Double click";
            d.id = id;
            d.style.left = x + "px";
            d.style.top = y + "px";
        }
        else {
            span.innerHTML = load[1];
            d.id = load[0];
            d.style.top = y;
            d.style.left = x;
            d.style.height = load[2] + "px";
            d.style.width = load[3] + "px";
        }
        instance.getContainer().appendChild(d);
        initNode(d, true);
        return d;
    };
    var loadNode = function(shape, x, y, id, text, h, w) {
        var d = document.createElement("div");
        var ep = document.createElement("div");
        d.classList.add("window");
        if (shape == "rectangle") d.classList.add("rectangle");
        if (shape == "round") d.classList.add("round");
        if (shape == "ellipsis") d.classList.add("ellipsis");
        d.id = id;
        var span = document.createElement("span");
        span.classList.add("windowText");
        ep.classList.add("ep");
        span.innerHTML = text;
        d.appendChild(ep);
        var img = document.createElement("img");
        img.src = "../images/arrow.png";
        img.width = "30px";
        img.height = "15px";
        ep.appendChild(img);
        d.appendChild(span);
        d.style.left = x;
        d.style.top = y;
        //d.style.height = h + "px";
        //d.style.width = w + "px";
        instance.getContainer().appendChild(d);
        //nodes.push(d);
        return d;
    };
    //var loadConnections = function
    instance.batch(function () {
        //load function here
    });

    jsPlumb.fire("jsPlumbLoaded", instance);


});





